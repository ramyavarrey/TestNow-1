module TestNow

  TAKE_A_TOUR_DASHBOARD = {
      0 => "Welcome to TestNow Tour!",
      1 => "Menu",
      2 => "Profile",
      3 => "Dashboard",
      # 6 => "URL Test",
      # 7 => "Manual Test",
      6 => "Automated Test",
      7 => "ScaleNow",
      8 => "Documentation",
      9 => "Overview",
      10 => "Last job execution time",
      11 => "Last Failed Fast Time",
      12 => "Test Cases Passed",
      13 => "Releasable Builds",
      14 => "bubbles",
      15 => "Current Status with count",
      16 => "Toggle Overview",
      17 => "Group By Menu",
      18 => "Toggle Last job List",
      19 => "Show Consolidated Report"
  }

  TAKE_A_TOUR_REPORT = {
      0 => "Welcome to TestNow Tour!",
      1 => "Menu",
      2 => "Profile",
      3 => "Dashboard",
      # 6 => "URL Test",
      # 7 => "Manual Test",
      6 => "Automated Test",
      7 => "ScaleNow",
      8 => "Documentation"
      # 22 => "Welcome to Consolidated Report",
      # 23 => "Test Success Rate",
      # 25 => "Run Details By Browser",
      # 26 => "Test Details"
  }

  TAKE_A_TOUR_LOGIN = {
      0 => "Signup",
      1 => "Login",
      2 => "Home",
      4 => "Documentation",
      5 => "Blog",
      6 => "TestNow Video",
      7 => "Opex Software",
  }

  HELP_TEXT_ARR = ["Manual" , "URL" , "use" , "auto", "cal", "perf", "Q", "selenium"]

end