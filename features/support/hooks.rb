Before do
  browser = ENV['BROWSER'].downcase
   @test_data = YAML.load_file(File.dirname(__FILE__) + "/../../data/test_data.yml")
  case browser
    when 'chrome'
      launch_driver_chrome
    when 'firefox'
      launch_driver_firefox
    when 'ie'
      launch_driver_ie
    when 'opera'
      launch_driver_opera
  end
end

After do |scenario|
  if scenario.failed?
    begin
      encoded_img = driver.screenshot_as(:base64)
      embed("data:image/png;base64,#{encoded_img}", "image/png")
    rescue
      p "*** Could not take failed scenario screenshot ***"
    end
  end
  quit_driver
end