require 'rubygems'
require 'rspec'
require 'selenium-webdriver'
require 'cucumber'
require 'rake'
require 'faker'
require 'yaml'
require 'selenium_fury'

include RSpec::Matchers
include SeleniumFury::SeleniumWebDriver::CreateSeleniumWebDriver

require File.dirname(__FILE__) + "/../support/driver_config"
require File.dirname(__FILE__) + "/../support/utilities"
require File.dirname(__FILE__) + "/../../data/runtime_data"
include TestNow