module TestNow

  def select_random_value_from_dropdown_using_xpath(select_box_xpath)
    options = @driver.find_elements(xpath: select_box_xpath)
    value = options[rand(1..options.size-1)].text
    p "****** Random Value selected is #{value} ******"
    new_select_xpath = select_box_xpath.split("/option")[0]
    select_drop_down_by_xpath(new_select_xpath,value)
    #@driver.find_element(xpath: new_select_xpath).send_keys(value)
    return value
  end

  def select_drop_down_by_xpath drop_down ,text
    Selenium::WebDriver::Support::Select.new(driver.find_element(:xpath => drop_down)).select_by :text, text
  end

  def upload_file(id,file_path)
    p "*** uploading file (#{file_path}) ***"
    @driver.find_element(id: "#{id}").send_keys(file_path)
  end

  def scroll_to_top_of_page
    @driver.execute_script("window.scrollTo(0,-9999)")
  end

  def scroll_to_bottom_of_page
    @driver.execute_script("window.scrollTo(0,9999)")
  end

end