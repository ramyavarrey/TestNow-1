# Firefox browser
def launch_driver_firefox
  @driver = Selenium::WebDriver.for :firefox
  @driver.manage.timeouts.implicit_wait = 30
  #@driver.manage.timeouts.page_load = 120
  @driver.manage.window.maximize
end

def launch_driver_chrome
  @driver = Selenium::WebDriver.for :chrome
  @driver.manage.timeouts.implicit_wait = 30
  #@driver.manage.timeouts.page_load = 120
  @driver.manage.window.maximize
end

def launch_driver_opera
  client = Selenium::WebDriver::Remote::Http::Default.new
  client.timeout = 180 # seconds
  service = Selenium::WebDriver::Chrome::Service.new("/usr/local/bin/operadriver", 48923)
  service.start
  cap = Selenium::WebDriver::Remote::Capabilities.chrome('operaOptions' => {'binary' => '/usr/bin/opera', 'args' => ["--ignore-certificate-errors"]})
  @driver = Selenium::WebDriver.for(:remote, :url => service.uri, :desired_capabilities => cap, :http_client => client)
  @driver.manage.timeouts.implicit_wait = 60
  @driver.manage.window.maximize
  @driver.manage.timeouts.page_load = 120
end

def driver
  @driver
end

def close_driver
  @driver.close
end

def quit_driver
  @driver.quit
end

