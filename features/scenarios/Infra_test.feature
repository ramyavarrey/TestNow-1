
Feature: Validating the Infra test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  Scenario: I verify the serverspec functioanlity with Default Packages
    Given I navigate to "Infra Test" tab from header
    When I select "Test" from Infra tests dropdown
    And I select the Spec Type as Serverspec
    And I fill in the spec details with password
    And I select the Use Default Packages checkbox
    And I click next button on SpecDetails form
    When I fill in the infra test Codebase form
    Then I click next button on Codebase form
    When I fill in the infra test ExecutionDetails form
    Then I submit the infra test job
    And I wait for job to be submitted

  Scenario: I verify the serverspec functioanlity with Authenticity Type as Password
    Given I navigate to "Infra Test" tab from header
    When I select "Test" from Infra tests dropdown
    And I select the Spec Type as Serverspec
    And I fill in the spec details with password
    And I click next button on SpecDetails form
    When I fill in the infra test Codebase form
    Then I click next button on Codebase form
    When I fill in the infra test ExecutionDetails form
    Then I submit the infra test job
    And I wait for job to be submitted


  Scenario: I verify the serverspec functioanlity with Authenticity Type as Key
    Given I navigate to "Infra Test" tab from header
    When I select "Test" from Infra tests dropdown
    And I select the Spec Type as Serverspec
    And I fill in the spec details with key
    And I click next button on SpecDetails form
    When I fill in the infra test Codebase form
    Then I click next button on Codebase form
    When I fill in the infra test ExecutionDetails form
    Then I submit the infra test job
    And I wait for job to be submitted

  Scenario: I verify the awspec functionality of Awspec
    Given I navigate to "Infra Test" tab from header
    When I select "Test" from Infra tests dropdown
    And I select the Spec Type as Awspec
    And I fill in the Awspec details form
    And I click next button on SpecDetails form
    When I fill in the infra test Codebase form
    Then I click next button on Codebase form
    When I fill in the infra test ExecutionDetails form
    Then I submit the infra test job
    And I wait for job to be submitted


  Scenario: Verify you are able to upload the automation code as zip from Upload Code
    Given I navigate to "Infra Test" tab from header
    When I select "Test" from Infra tests dropdown
    And I select the Spec Type as Serverspec
    And I fill in the spec details with key
    And I click next button on SpecDetails form
    When I select empty file in upload infra test code
    Then I verify zip file uploaded fails
    And I select upload infra test code from Codebase form
    Then I verify zip file uploaded successfully
    And I click next button on Codebase form
    When I fill in the infra test ExecutionDetails form
    Then I submit the infra test job
    And I wait for job to be submitted


  Scenario: Validation error checks and disability of submit button until form filled correctly
    Given I navigate to "Infra Test" tab from header
    When I select "Test" from Infra tests dropdown
    When I send space in Job Name field on infra test SpecDetails form
    Then I verify error message for Job Name field
    When I send space in IP Address field on infra test SpecDetails form
    Then I verify error message for IP Address field
    When I send space in user field on infra test SpecDetails form
    Then I verify error message for user field
    When I send space in password field on infra test SpecDetails form
    Then I verify error message for password field
    And I select authentication type as key
    When I send space in key field on infra test SpecDetails form
    Then I verify error message for key field
    Then I click next button on SpecDetails form

    When I send space in GitUrl field on infra test Codebase form
    Then I verify error message for GitUrl field
    Then I click next button on Codebase form

    When I send space in TestRunCmd field on infra test ExecutionDetails form
    Then I verify error message for TestRunCmd field

    Then I verify submit button is disable


  Scenario: I verify the serverspec functioanlity with Default Packages
    Given I navigate to "Infra Test" tab from header
    When I select "Test" from Infra tests dropdown
    And I select the Spec Type as Serverspec
    And I fill in the spec details with password
    And I select the Use Default Packages checkbox
    And I click next button on SpecDetails form
    When I fill in the infra test Codebase form
    Then I click next button on Codebase form
    When I fill in the infra test ExecutionDetails form
    Then I submit the infra test job
    And I wait for job to be submitted