Feature: TestNow Login Scenario

  Background:
    Given I am on TestNow home page

  Scenario: Login And Logout From Testnow Portal
    When I follow "login" button
    And I login to TestNow portal
    Then I verify successful login to TestNow portal
    And I logout form testnow portal
    Then I verify successful logout from TestNow portal


  Scenario: Login with invalid credentials
    When I follow "login" button
    And I login to TestNow portal with invalid credentials
    Then I verify error message on the TestNow page


  Scenario: Successful signup with a valid user
    When I follow "signup" button
    And I fill the signup form
    When I clicked Sign Up button
    Then I verify the user successfully logged in with email verification link

    
  Scenario: Sign Up With Already Registered Email ID
    When I follow "signup" button
    And I signup with a current user
    Then I verify email is already registered with user


  Scenario: Sign Up With Invalid Password
    When I follow "signup" button
    And I signup with a invalid "less digit" password
    Then I verify "less digit" password error
    And I signup with a invalid "more digit" password
    Then I verify "more digit" password error
    And I signup with a invalid "alphanumeric" password
    Then I verify "alphanumeric" password error

  Scenario:SignUp With Empty Fields
    When I follow "signup" button
    And I left the fields empty
    Then I Verify the disabled Sign Up button

  Scenario:SignUp Without selecting the Terms and Conditions checkbox
    When I follow "signup" button
    And I enter all the details without selecting the checkbox
    Then I Verify the disabled Sign Up button


  Scenario: Validation error verification for Signup form
    When I follow "signup" button
    When I send space in Name field on Signup form
    Then I verify error message for Name field on Signup page
    When I send space in Email field on Signup form
    Then I verify error message for Email field on Signup page
    When I send space in Password field on Signup form
    Then I verify error message for Password field on Signup page
    When I send space in Organization field on Signup form
    Then I verify error message for Organization field on Signup page

  Scenario: Validation error for email address for Signup form
    When I follow "signup" button
    And I enter email address as text
    Then I verify error message for Email field on Signup page
    And I enter email address as hello@gmail.
    Then I verify error message for Email field on Signup page
    And I enter email address as text@gmailcom
    Then I verify error message for Email field on Signup page
    And I enter email address as text@gmail
    Then I verify error message for Email field on Signup page
    And I enter email address as text@fdhghf
    Then I verify error message for Email field on Signup page



  Scenario: Skipping the AWS Details Form
    When I follow "login" button
    And I login to TestNow portal
    And I do not fill any of the details in the form
    And I Click on Skip button
    And I hit OK on pop up message
    Then I should be on the "dashboard" page


  Scenario: Filling in the AWS Details
    When I follow "login" button
    And I login to TestNow portal
    And I fill in the AWS Details
    And I Click on Save button
    Then I verify the successfull message of saving AWS Details