Feature: Validating the Automated test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal



  Scenario: Submit an Automated test job and wait for it to complete
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
     | Firefox | Chrome | UIPerfAnalysis |
     | RANDOM  | RANDOM | UPA            |
    And I fill in the automated test SelectBrowser form
    Then I click next button on SelectBrowser form
    When I fill in the automated test Codebase form
    Then I click next button on Codebase form
    When I fill in the automated test ExecutionDetails form
    Then I submit the automated test job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I verify 3 number of browser bubbles
    Then I wait for Automated job to complete with 3 browser


  Scenario: Check the "Try with sample code" functionality for RUBY
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    When I click on sample Ruby code link
    Then I verify all data in summary table is as follows:
      | Key                       | Value                                        |
      | Job Name                  | magento_test                                 |
      | Application URL           | http://54.89.102.77/                         |
      | Git Repo                  | https://github.com/                          |
      | Git branch                | master                                       |
      | Test Run Command          | cucumber features                            |
      | Output Directory          | reports                                      |
      | Report File               | magento_report.json                          |
      | Preserve machine if failed | false                                       |
    When I click next button on SelectBrowser form
    And I click next button on Codebase form
    And I submit the automated test job
    Then I wait for SelectBrowsers error message to be displayed
    When I click previous button on ExecutionDetails form
    And I click previous button on Codebase form
    And I select browsers as follows:
      | Firefox |
      | RANDOM  |
    When I click next button on SelectBrowser form
    And I click next button on Codebase form
    And I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser



  Scenario: Check the "Try with sample code" functionality for JAVA
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
      | Firefox |
      | RANDOM  |
    When I click on sample Java code link
    Then I verify all data in summary table is as follows:
     # | Key                       | Value                                       |
      | Job Name                  | magento_test                                 |
      | Application URL           | http://54.89.102.77/                         |
      | Git Repo                  | https://github.com/opexsw                    |
      | Git branch                | master                                       |
      | Test Run Command          | mvn test                                     |
      | Pre - script              | clean install                                |
      | Output Directory          | target/reports                               |
      | Report File               | index.json                                   |
      | Preserve machine if failed | false                                       |
    Then I click next button on SelectBrowser form
    When I fill in the automated test Codebase form
    And I click next button on Codebase form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser

  Scenario: Check the "Try with sample code" functionality for HP-UFT
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
      | Firefox |
      | RANDOM  |
    When I click on sample HP-UFT code link
    Then I click next button on SelectBrowser form
    And I click next button on Codebase form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser


  Scenario: Submit an Automated test job with HP-UFT code and wait for it to complete
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
      | IE  |
      | 11  |
    And I fill in the automated test SelectBrowser form
    Then I click next button on SelectBrowser form
    When I fill in the automated test Codebase form with HP-UFT code
    Then I click next button on Codebase form
    When I fill in the automated test ExecutionDetails form with HP-UFT code
    Then I submit the automated test job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I verify 1 number of browser bubbles
    Then I wait for Automated job to complete with 1 browser

  Scenario: Verify no entries for automated tests under "Automated Test VMs" tab on  Manual Test Page
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
      | Firefox |
      | RANDOM  |
    And I fill in the automated test SelectBrowser form
    Then I verify preserve VMs checkbox is disable
    When I navigate to "Manual Test" tab from header
    And I click on Automation test VMs tab of Manual Test
    Then I verify Automation test VMs tab of Manual Test is blank


  Scenario: Verify Preserve VM checkbox is disabled by default
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
      | Firefox |
      | RANDOM  |
    And I fill in the automated test SelectBrowser form
    Then I verify preserve VMs checkbox is disable
    Then I verify submit button is disable



  Scenario: Validation error checks and disability of submit button until form filled correctly
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    When I send space in AppName field on automated test SelectBrowser form
    Then I verify error message for AppName field
    When I send space in AppUrl field on automated test SelectBrowser form
    Then I verify error message for AppUrl field
    Then I click next button on SelectBrowser form

    When I send space in GitUrl field on automated test Codebase form
    Then I verify error message for GitUrl field
    Then I click next button on Codebase form

    When I send space in TestRunCmd field on automated test ExecutionDetails form
    Then I verify error message for TestRunCmd field

    Then I verify submit button is disable


  Scenario: Verify the progress bar of automated test with each button click
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    When I click on SelectBrowser of progress bar
    Then I verify I am on SelectBrowser form on Automated Test page
    When I click on Codebase of progress bar
    Then I verify I am on Codebase form on Automated Test page
    When I click on ExecutionDetails of progress bar
    Then I verify I am on ExecutionDetails form on Automated Test page


  Scenario: Verify you are able to upload the automation code as zip from Upload Code
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
      | Firefox |
      | RANDOM  |
    And I fill in the automated test SelectBrowser form
    Then I click next button on SelectBrowser form
    When I select empty file in upload automated test code
    Then I verify zip file uploaded fails
    And I select upload automated test code from Codebase form
    Then I verify zip file uploaded successfully
    And I click next button on Codebase form
    When I fill in the automated test ExecutionDetails with prescript form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser


  Scenario: Verify the error message for not entering the Output Directory
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    When I click on sample Ruby code link
    When I click next button on SelectBrowser form
    And I click next button on Codebase form
    And I submit the automated test job
    Then I wait for SelectBrowsers error message to be displayed
    When I click previous button on ExecutionDetails form
    And I click previous button on Codebase form
    And I select browsers as follows:
      | Firefox |
      | RANDOM  |
    When I click next button on SelectBrowser form
    And I click next button on Codebase form
    Then I clear the data in Output Directory
    And I submit the automated test job
    And I select OK button to submit job
    Then I wait for OutputDirectory error message to be displayed
    And I fill the Output Directory field
    And I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser









