Feature: Validating the UI Performance Analysis Test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal



  Scenario: Submit a UPA test job and wait for it to complete
    Given I navigate to "UI performance Analysis Test" tab from header
    And I fill in the UPA test form
    When I submit the UPA test
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for UPA job to complete with 1 browser


  Scenario: Submit a UPA test job with execution strategy as Boost
    Given I navigate to "UI performance Analysis Test" tab from header
    And I fill in the UPA test form by selecting execution strategy as boost
    When I submit the UPA test
    Then I wait for job to be submitted with successful message


  Scenario: Validation error verification for UI performance Analysis Test form
    Given I navigate to "UI performance Analysis Test" tab from header
    When I send space in AppUrl field on UPA test form
    Then I verify error message for AppUrl field on UPA Test page
    When I send space in TextToSearch field on UPA test form
    Then I verify error message for TextToSearch field on UPA Test page
    When I send space in Timeout field on UPA test form
    Then I verify error message for Timeout field on UPA Test page
    When I send alphabet in AppUrl field on UPA test form
    Then I verify error message for AppUrl field on UPA Test page
    When I send negative in Timeout field on UPA test form
    Then I verify error message for Timeout field on UPA Test page


