Feature: Validating Help in TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  Scenario: Validating links on Help page
    Given I navigate to "Help" tab from header
    Then I verify each links on help page

  Scenario: Validating search on Help page
    Given I navigate to "Help" tab from header
    Then I verify search text on help page

  Scenario: Validating FAQ's on Help page
    Given I navigate to "Faqs" tab from header
    And I verify each faqs on help page
    Then I verify all faqs on help page

