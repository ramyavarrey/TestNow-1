Feature: Validating the URL test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal


  Scenario: Submit a URL test job with crawling URL and wait for it to complete
      Given I navigate to "URL Test" tab from header
      And I select browsers as follows:
        | Firefox | Chrome | Opera | IE |  UIPerfAnalysis |  Device     |
        | 43      | 47     | 30    | 10 |  UPA            |  RANDOM     |
      And I fill in the url test form
      And I checked crawl url checkbox
      When I submit the url test
      And I wait for job to be submitted
      And I navigate to "Dashboard" tab from header
      Then I wait for URL job to complete with 6 browser




  Scenario: Submit a URL test job with execution strategy as efficient
      Given I navigate to "URL Test" tab from header
      And I select browsers as follows:
      | Firefox | Chrome | Opera | IE |  UIPerfAnalysis |  Device     |
      | 43      | 47     | 30    | 10 |  UPA            |  RANDOM     |
      And I fill in the url test form by selecting execution strategy as efficient
      And I checked crawl url checkbox
      When I submit the url test
      Then I wait for job to be submitted with successful message


  Scenario: Validation error verification for URL Test form
    Given I navigate to "URL Test" tab from header
    When I send space in AppUrl field on url test form
    Then I verify error message for AppUrl field on URL Test page
    When I send space in TextToSearch field on url test form
    Then I verify error message for TextToSearch field on URL Test page
    When I send space in Timeout field on url test form
    Then I verify error message for Timeout field on URL Test page
    When I send alphabet in AppUrl field on url test form
    Then I verify error message for AppUrl field on URL Test page
#    When I send alphabet in Timeout field on url test form
#    Then I verify error message for Timeout field on URL Test page
    When I send negative in Timeout field on url test form
    Then I verify error message for Timeout field on URL Test page

    When I send space in Timeout field on url test form
    Then I verify error message for Timeout field on URL Test page
    And I verify TestNow button is disabled
    When I fill in the url test form
    And I submit the url test
    Then I wait for SelectBrowsers error message to be displayed
