Feature: Validating the ScaleNow functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal


  Scenario: Submit an ScaleNow job and wait for it to complete
    Given I navigate to "ScaleNow" tab from header
    And I select browsers as follows:
      | Firefox | Chrome | Opera  |
      | RANDOM  | RANDOM | RANDOM |
    And I fill in the scalenow test SelectBrowser form
    Then I click next button on SelectBrowser form
    And I fill in the scalenow test Codebase form
    Then I click next button on Codebase form
    And I fill in the scalenow test ExecutionDetails form
    Then I submit the scalenow test job
    And I navigate to "Dashboard" tab from header
    Then I wait for Scale job to complete with 1 browser
    When I click ScaleNowReport button on dashboard page
    Then I verify ScaleNow Report


  Scenario: Verify the values for scalenow job on RHS pannel
    Given I navigate to "ScaleNow" tab from header
    And I fill in the scalenow test SelectBrowser form
    Then I click next button on SelectBrowser form
    And I fill in the scalenow test Codebase form
    Then I click next button on Codebase form
    And I fill in the scalenow test ExecutionDetails form
    Then I submit the scalenow test job
    Then I wait for SelectBrowsers error message to be displayed
    When I click previous button on ExecutionDetails form
    And I click previous button on Codebase form
    And I select browsers as follows:
      | Firefox | Chrome | Opera  |
      | RANDOM  | RANDOM | RANDOM |
    Then I verify all data in summary table is as follows:
      | Key                       | Value                                        |
      | Job Name                  | GoogleTest                                   |
      | Application URL           | http://www.google.com                        |
      | Parallel VMs Count        | 1                                            |
      | Hours to Run              | 1                                            |
      | Use Upload Code           | false                                        |
      | Git Repo                  | https://bitbucket.org/gan                    |
      | User ID                   | ganeshkhakare                                |
      | Git branch                | master                                       |
      | Test Run Command          | features_report.htmlfeatures_report.json --format Formatter::HtmlFormatter --out=reportgmail.feature -p selenium --format pretty --format json --out=reportcucumber features           |
      | Pre - script              | export TEXT_TO_SEARCH=Gmail                  |
      | Post - script             | echo "Executing PostScript"                  |
      | Output Directory          | report                                       |
      | Report File               | features_report.json                         |



  Scenario: Verify the error messages on all mandatory field if it is left empty
    Given I navigate to "ScaleNow" tab from header
    When I send space in AppName field on scalenow SelectBrowser form
    Then I verify error message for AppName field on scalenow form
    When I send space in AppUrl field on scalenow SelectBrowser form
    Then I verify error message for AppUrl field on scalenow form
    When I send space in Parallel Users Count field on scalenow SelectBrowser form
    Then I verify error message for Parallel Users Count field on scalenow form
    When I send space in Hours To Run field on scalenow SelectBrowser form
    Then I verify error message for Hours To Run field on scalenow form
    Then I click next button on SelectBrowser form

    When I send space in GitUrl field on scalenow Codebase form
    Then I verify error message for GitUrl field on scalenow form
    Then I click next button on Codebase form

    When I send space in TestRunCmd field on scalenow ExecutionDetails form
    Then I verify error message for TestRunCmd field on scalenow form
    Then I verify ScaleNow button is disabled

  Scenario: Verify the progress bar of automated test with each button click
    Given I navigate to "ScaleNow" tab from header
    When I click on SelectBrowser of progress bar
    Then I verify I am on SelectBrowser form on Load Test page
    When I click on Codebase of progress bar
    Then I verify I am on Codebase form on Load Test page
    When I click on ExecutionDetails of progress bar
    Then I verify I am on ExecutionDetails form on Load Test page

  Scenario: Verify you are able to upload the scalenow code as zip from Upload Code
    Given I navigate to "ScaleNow" tab from header
    And I select browsers as follows:
      | Firefox | Chrome | Opera  |
      | RANDOM  | RANDOM | RANDOM |
    And I fill in the scalenow test SelectBrowser form
    Then I click next button on SelectBrowser form
    And I select upload scalenow code from Codebase form
    Then I click next button on Codebase form
    And I fill in the scalenow test ExecutionDetails form
    And I submit the scalenow test job
    Then I verify file uploded successfully


  Scenario: I Verify the ScaleNow job in the last few jobs section
    Given I navigate to "ScaleNow" tab from header
    And I select browsers as follows:
      | Firefox | Chrome | Opera  |
      | RANDOM  | RANDOM | RANDOM |
    And I fill in the scalenow test SelectBrowser form
    Then I click next button on SelectBrowser form
    And I fill in the scalenow test Codebase form
    Then I click next button on Codebase form
    And I fill in the scalenow test ExecutionDetails form
    Then I submit the scalenow test job
    And I navigate to "Dashboard" tab from header
    When I search for SCALENOW job id in dashboard
    Then I verify one row display with SCALENOW job id