Feature: Navigation to different TestNow pages

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  Scenario: Navigating to different header and footer links
    Given I should be on the "Dashboard" page
    When I navigate to "URL Test" tab from header
    Then I should be on the "URL Test" page
    When I navigate to "Manual Test" tab from header
    Then I should be on the "Manual Test" page
    When I navigate to "Security Test" tab from header
    Then I should be on the "Security Test" page
    When I navigate to "UI Performance Analysis Test" tab from header
    Then I should be on the "UI Performance Analysis Test" page
    When I navigate to "Cross Browser" tab from header
    Then I should be on the "Cross Browser" page
#    When I navigate to "Mobile App" tab from header
#    Then I should be on the "Mobile App" page
 #   When I navigate to "Rest API" tab from header
 #   Then I should be on the "Rest API" page
    When I navigate to "Load Test" tab from header
    Then I should be on the "Load Test" page
    When I navigate to "Help" item from header
    Then I should be on the "Help" page
#    When I navigate to "Configure TestNow" item from header
#    Then I should be on the "Configure TestNow" page
    When I navigate to "FAQs" item from header
    Then I should be on the "FAQs" page
    When I navigate to "Profile" item from header
    Then I should be on the "Profile" page
    When I navigate to "Usage" item from header
    Then I should be on the "Usage" page
#    When I navigate to "Balance Hours" item from header
#    Then I should be on the "Balance Hours" page

    When I navigate to "Terms & Conditions" tab from footer
    Then I should be on the "Terms & Conditions" page
    When I navigate to "Powered By" tab from footer
    Then I should be on the "Rean Cloud" page
    #Then I should be on the "Opex Software" page
    When I navigate to "Jenkins Plugin" tab from footer
    Then I should be on the "Help Jenkins Plugin" page

    

  Scenario: I verify the take a tour functionality both front-back and back-front on various pages
    When I follow take-a-tour link from inside
    Then I verify take-a-tour on dashboard page using start-end approach
    And I verify take-a-tour on dashboard page using end-start approach
    And I end-tour and verify tour has ended

#    Given I toggle open the Last Few Jobs section
    When I select URL test job id from list of jobs and fetch job id
    And I follow consolidated report
    When I follow take-a-tour link from inside
    Then I verify take-a-tour on report page using start-end approach
    And I verify take-a-tour on report page using end-start approach
    And I end-tour and verify tour has ended

    When I logout form testnow portal
    Then I verify successful logout from TestNow portal
    When I follow take-a-tour link from outside
    Then I verify take-a-tour on login page using start-end approach
    And I verify take-a-tour on login page using end-start approach
    And I end-tour and verify tour has ended

