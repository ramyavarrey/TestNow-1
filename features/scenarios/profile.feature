Feature: As a user I should be able to view and edit my profile

  Background:
    Given I am on TestNow home page
    And I follow "login" button
    And I login to TestNow portal
    When I navigate to "Profile" item from header


  Scenario: Verify users profile details are populated and non-editable by default
    Then I should see profile section
    And I should see profile details populated
    And I verify profile details to be non-editable
    When I click on "Edit Profile" button on profile page
    Then I verify profile details to be editable

#  Scenario: Verify referral program on profile page
#    And I should see referral section
#    When I fill "invalid" email in email id
#    Then I verify error message for invalid email id field
#    And I fill "invalid@" email in email id
#    Then I verify error message for invalid email id field
#    When I fill "valid" email in email id
#    Then I verify error message for valid email id field



