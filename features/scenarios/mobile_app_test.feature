Feature: Validating the Mobile App Automated test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  @wip
  Scenario: Validation error checks and disability of submit button until form filled correctly for mobile app test
    Given I navigate to "Automated Test" tab from header
    When I select "Mobile App" from Automated tests dropdown
    When I send space in AppName field on automated test SelectBrowser form
    Then I verify error message for AppName field
    And I click next button on SelectBrowser form
    When I send space in GitUrl field on automated test Codebase form
    Then I verify error message for GitUrl field
    Then I click next button on Codebase form
    When I send space in TestRunCmd field on automated test ExecutionDetails form
    Then I verify error message for TestRunCmd field
    Then I verify submit button is disable

  @wip
  Scenario: Submit an Automated test job for mobile app test and wait for it to complete
    Given I navigate to "Automated Test" tab from header
    When I select "Mobile App" from Automated tests dropdown
    And I select browsers as follows:
      | Android |
      | RANDOM |
    When I click on sample Appium code link
    Then I click next button on SelectBrowser form
    And I click next button on Codebase form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser

  @wip
  Scenario: Check the "Try with sample code" functionality for Appium
    Given I navigate to "Automated Test" tab from header
    When I select "Mobile App" from Automated tests dropdown
    When I click on sample Appium code link
    Then I verify all data in summary table is as follows:
      | Key                       | Value                                        |
      | Job Name                  | Appium                                       |
      | Git Repo                  | https://github.com/                          |
      | Git branch                | master                                       |
      | Test Run Command          | mvn test                                     |
      | Output Directory          | AppiumProject/target/                        |
      | Report File               | testng-results.xml                           |
      | Preserve machine if failed | false                                       |
    When I click next button on SelectBrowser form
    And I click next button on Codebase form
    And I submit the automated test job
    Then I wait for SelectBrowsers error message to be displayed
    When I click previous button on ExecutionDetails form
    And I click previous button on Codebase form
    And I select browsers as follows:
      | Android |
      | RANDOM  |
    Then I click next button on SelectBrowser form
    And I click next button on Codebase form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser

  @wip
  Scenario: Verify the progress bar of mobile app automated test with each button click
    Given I navigate to "Automated Test" tab from header
    When I select "Mobile App" from Automated tests dropdown
    When I click on SelectVersion of progress bar
    Then I verify I am on SelectBrowser form on Automated Test page
    When I click on Codebase of progress bar
    Then I verify I am on Codebase form on Automated Test page
    When I click on ExecutionDetails of progress bar
    Then I verify I am on ExecutionDetails form on Automated Test page

  @wip
  Scenario: Verify you are able to upload the mobile app automation code as zip from Upload Code
    Given I navigate to "Automated Test" tab from header
    When I select "Mobile App" from Automated tests dropdown
    And I select browsers as follows:
      | Android |
      | RANDOM  |
    When I click on sample Appium code link
    Then I click next button on SelectBrowser form
    And I select upload mobile app test code from Codebase form
    And I click next button on Codebase form
    When I add prescript in mobile app test form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser