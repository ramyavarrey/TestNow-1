Feature: Validating the Manual test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  Scenario: Create a new manual browser and wait for it to provision
    Given I delete all extra manual test machine present
    And I select browsers as follows:
      | Chrome |Firefox |
      | RANDOM |RANDOM  |
#      | Firefox | Chrome | Opera  | OperaMini | OperaMobile | IE     |
#      | RANDOM  | RANDOM | RANDOM | RANDOM    | RANDOM      | RANDOM |
    When I fill in the manual test form
    And I select resolution with RANDOM value
    And I start the manual browser creation process
    Then I wait for manual browser to be submitted
    And I wait for manual browser to provision
    Then I verify table contents with run details

  Scenario: Sleep-Restore a manual browser machine
    Given I navigate to "Manual Test" tab from header
    And I sleep the manual browser
    And I wait for manual process to be submitted
    And I wait for manual browser to sleep
    When I restore the manual browser
    And I wait for manual process to be submitted
    And I wait for manual browser to restore

  Scenario: Validation of error messages
    Given I navigate to "Manual Test" tab from header
    When I fill in the manual test form with nil value
    Then I verify Start Now button is disabled
    And I select resolution with RANDOM value
    Then I verify Start Now button is disabled

    When I fill in the manual test form
    And I select resolution with RANDOM value
    And I start the manual browser creation process
    Then I wait for SelectBrowsers error message to be displayed

 Scenario: Delete a manual browser machine
   Given I delete all extra manual test machine present

