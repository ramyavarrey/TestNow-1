Feature: Validating the usage statistics are genuinely calculated

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  Scenario Outline: Verify usage statistics as per Automated test job in dashboard
#    Given I toggle open the Last Few Jobs section
    And I select <job_type> test job id from list of jobs and fetch job id
    When I count the number of browser bubbles
    And I navigate to "Usage" tab from header
    And I search for job id in run number column
    Then I verify usage statistic as per <job_type> test job in dashboard
  Examples:
    | job_type  |
    | AUTOMATED |
    | URL       |

  Scenario: Verify sorted order Usage time for Automation Tests
    When I navigate to "Usage" tab from header
    And I click on the automated tests usage time column header
    Then I verify automated usage time column is sorted in "asc" order
    And I click on the automated tests usage time column header
    Then I verify automated usage time column is sorted in "desc" order

  Scenario: Verify sorted order Usage time for Manual Tests
    When I navigate to "Usage" tab from header
    And I click on manual tests tab of manual tests page
    And I click on the manual tests usage time column header
    Then I verify manual usage time column is sorted in "asc" order
    And I click on the manual tests usage time column header
    Then I verify manual usage time column is sorted in "desc" order

  Scenario: Verify UI Sections on Usage Page
    When I navigate to "Usage" tab from header
    Then I verify "Usage Details for Last 30 days" title on usage page
    And I verify all sections of usage page are displayed

  Scenario: Automation filter for job type on usage page
    When I navigate to "Usage" tab from header
    And I select job type dropdown on usage page
    Then I verify selected job type is display in job type column






