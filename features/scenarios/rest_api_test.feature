Feature: Validating the Rest API Automated test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  Scenario: Validation error checks and disability of submit button until form filled correctly for mobile app
    Given I navigate to "Automated Test" tab from header
    When I select "Rest API" from Automated tests dropdown
    When I send space in AppName field on automated test SelectBrowser form
    Then I verify error message for AppName field
    When I send space in AppUrl field on automated test SelectBrowser form
    Then I verify error message for AppUrl field
    Then I click next button on SelectBrowser form
    When I send space in GitUrl field on automated test Codebase form
    Then I verify error message for GitUrl field
    Then I click next button on Codebase form
    When I send space in TestRunCmd field on automated test ExecutionDetails form
    Then I verify error message for TestRunCmd field
    Then I verify submit button is disable

  Scenario: Submit Rest API Automated test and wait for it to complete
    Given I navigate to "Automated Test" tab from header
    When I select "Rest API" from Automated tests dropdown
    When I click on sample Rest API code link
    Then I click next button on SelectBrowser form
    And I click next button on Codebase form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser

  Scenario: Check the "Try with sample code" functionality for Rest API
    Given I navigate to "Automated Test" tab from header
    When I select "Rest API" from Automated tests dropdown
    When I click on sample Rest API code link
    Then I verify all data in summary table is as follows:
      | Key                       | Value                                        |
      | Job Name                  | RestAPITesting                               |
      | Application URL           | https://api.github.com                       |
      | Git Repo                  | https://github.com/                          |
      | Git branch                | master                                       |
      | Test Run Command          | mvn surefire-report:report;                  |
      | Output Directory          | target/surefire-reports                      |
      | Report File               | TEST-com.APITest.xml                         |
      | Preserve machine if failed | false                                       |


  Scenario: Verify the progress bar of mobile app automated test with each button click
    Given I navigate to "Automated Test" tab from header
    When I select "Rest API" from Automated tests dropdown
    When I click on BaseUrl of progress bar
    Then I verify I am on SelectBrowser form on Automated Test page
    When I click on Codebase of progress bar
    Then I verify I am on Codebase form on Automated Test page
    When I click on ExecutionDetails of progress bar
    Then I verify I am on ExecutionDetails form on Automated Test page


  Scenario: Verify you are able to upload the mobile app automation code as zip from Upload Code
    Given I navigate to "Automated Test" tab from header
    When I select "Rest API" from Automated tests dropdown
    When I click on sample Rest API code link
    Then I click next button on SelectBrowser form
    And I select upload rest api test code from Codebase form
    And I click next button on Codebase form
    When I add prescript in rest api test form
    Then I submit the automated test job
    And I select OK button to submit job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Automated job to complete with 1 browser


