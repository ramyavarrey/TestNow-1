Feature: Validating the Security test functionality of TestNow

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal

  Scenario: Validation error verification for Security test form
    Given I navigate to "Security Test" tab from header
    When I send space in AppUrl field on security test form
    Then I wait for EnterValidURL message to be displayed

    And I send Valid App URL
    When I checked HttpHeader checkbox
    When I click TestNow button
    Then I wait for SelectBrowsers error message to be displayed

    And I select browsers as follows:
      | Chrome | Firefox|
      |   47   |   42   |
    And I send Valid App URL
    And I do not select any security packs
    Then I verify Test Now button is disabled


  Scenario: Submit a Security test job and wait for it to complete
    Given I navigate to "Security Test" tab from header
    And I select browsers as follows:
      | Chrome | Firefox|
      | 47:46  | 43:42  |
    And I send Valid App URL
    When I checked AppScan checkbox
    Then I verify AppScan packs
    When I checked HttpHeader checkbox
    Then I verify HttpHeader packs
    When I click TestNow button
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    Then I wait for Security job to complete with 4 browser



