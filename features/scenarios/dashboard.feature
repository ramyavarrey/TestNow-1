Feature: Validating dashboard page

  Background:
    Given I am on TestNow landing page
    And I follow "login" button
    When I login to TestNow portal


  Scenario: I verify the restart URL job functionality
 #  Given I toggle open the Last Few Jobs section
    When I hit restart button for latest URL job
    Then I should be on the "URL Test" page
    

  Scenario: I verify the restart AUTOMATED job functionality
#    Given I toggle open the Last Few Jobs section
    Given I navigate to "Automated Test" tab from header
    When I select "Cross Browser" from Automated tests dropdown
    And I select browsers as follows:
      | Firefox | Chrome | UIPerfAnalysis |
      | RANDOM  | RANDOM | UPA            |
    And I fill in the automated test SelectBrowser form
    Then I click next button on SelectBrowser form
    When I fill in the automated test Codebase form
    Then I click next button on Codebase form
    When I fill in the automated test ExecutionDetails form
    Then I submit the automated test job
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    When I hit restart button for latest AUTOMATED job
    Then I should be on the "Automated Test" page
    And I verify all data in summary table is as follows:
      | Key                       | Value                                       |
      | Job Name                  | magento_test                                 |
      | Application URL           | http://54.89.102.77/                         |
      | Git Repo                  | https://github.com/                          |
      | Git branch                | master                                       |
      | Test Run Command          | cucumber features                            |
      | Output Directory          | reports                                      |
      | Report File               | magento_report.json                          |






  Scenario: I verify presence of 4 header widgets
    Given I verify dashboard widgets are displayed

  Scenario: I verify the toggle functionality for last few jobs section
    Given I verify last few jobs section is open
    When I toggle open the Last Few Jobs section
    Then I verify last few jobs section is closed
    When I toggle close the Last Few Jobs section
    Then I verify last few jobs section is open
    When I toggle close the Last Few Jobs section
    Then I verify last few jobs section is closed


    Scenario: I search for last few jobs in dashboard
      When I search for URL job id in dashboard
      Then I verify one row display with URL job id
      When I search for AUTOMATED job id in dashboard
      Then I verify one row display with AUTOMATED job id

  Scenario: I verify the stop URL job functionality
    Given I navigate to "URL Test" tab from header
    And I select browsers as follows:
      | Firefox | Chrome |
      | 43      | 47     |
    And I fill in the url test form
    And I checked crawl url checkbox
    When I submit the url test
    And I wait for job to be submitted
    And I navigate to "Dashboard" tab from header
    When I hit stop button for latest URL job
    And I hit OK on stop job confirmation popup
    And I see URL job status as stopped



