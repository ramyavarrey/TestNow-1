When /^I fill in the manual test form(?: with (nil) value)?$/ do |values|
  manual = ManualTestPage.new(@driver, @test_data)
  manual.fill_manual_test_form(values)
end

When /^I select resolution with (RANDOM) value$/ do |resolution|
  manual = ManualTestPage.new(@driver, @test_data)
  manual.select_resolution(resolution)
end

When /^I start the manual browser creation process$/ do
  manual = ManualTestPage.new(@driver, @test_data)
  manual.manual_start_now_button.click
  sleep 2
end

When /^I wait for manual (process|browser|job) to be submitted$/ do |items|
  common = ManualTestPage.new(@driver,@test_data)
  case items
    when "browser"
      Selenium::WebDriver::Wait.new(timeout: 20).until {common.manual_test_job_submission_message.displayed?}
    when "process"
      Selenium::WebDriver::Wait.new(timeout: 20).until {common.manual_test_job_process_message.displayed?}
    when "job"
      Selenium::WebDriver::Wait.new(timeout: 20).until {common.manual_test_job_process_message.displayed?}
  end


end


Then /^I wait for manual browser to (provision|sleep|restore)$/ do |action|
  manual = ManualTestPage.new(@driver, @test_data)
  manual.wait_for_manual_browser_to(action)
end

Then /^I (delete|reset|sleep|restore) the manual browser$/ do |action|
  manual = ManualTestPage.new(@driver, @test_data)
  manual.perform_action_on_browser_machine(action)
end

Then /^I delete all extra manual test machine present$/ do
  step %{I navigate to "Manual Test" tab from header}
  total = @driver.find_elements(xpath: "//button[contains(text(),'Delete')]").size
  total.times do
    begin
      @driver.find_elements(id: "buttonDeleteNow").first.click
      sleep(1)
      @driver.find_element(xpath: "//button[text()='OK']").click
      step %{I wait for process to be submitted}
      sleep(3)
      @driver.navigate.refresh
    rescue
      p ":( :( :( --- Could not delete a manual test machine --- :( :( :("
    end
  end
end

And /^I click on Automation test VMs tab of Manual Test$/ do
  manual = ManualTestPage.new(@driver, @test_data)
  manual.manual_automation_test_vms_tab.click
end

Then /^I verify Automation test VMs tab of Manual Test is blank$/ do
  @driver.find_element(xpath: "//div[@class='table-responsive']/table/tbody").text.include?("").should == true
end

Then /^I verify Start Now button is disabled$/ do
  manual = ManualTestPage.new(@driver, @test_data)
  manual.manual_start_now_button.enabled?.should == false
end

Then /^I verify table contents with run details$/ do

  $bv_map.map do |browser,version|
    tmp_browser_name = "\'" + browser.to_s + ':' + version.to_s + "\'"
    p "Verifying table contents to include #{tmp_browser_name}"
      (@driver.find_element(xpath: "//DIV[@ng-switch-when='PROVISIONED'][contains(text(), #{tmp_browser_name})]")).nil?.should == false
  end
  #@driver.find_element(css: "tr[class='ng-scope']>td:nth-of-type(2)>div>div").text.include?(@test_data['MANUAL_TEST_NAME']).should == true
end
