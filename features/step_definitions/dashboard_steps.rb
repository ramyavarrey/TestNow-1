When /^I verify dashboard widgets are displayed$/ do
  dash = DashboardPage.new(@driver, @test_data)
  dash.verify_all_4_widgets_displayed
end

Given /^I toggle (?:open|close) the Last Few Jobs section$/ do
  dash = DashboardPage.new(@driver,@test_data)
  scroll_to_top_of_page
  dash.dashboard_toggle_jobs_section.click
  sleep(2)
end

When /^I select (AUTOMATED|URL) test job id from list of jobs and fetch job id$/ do |job_type|
  case job_type.downcase
    when "url"
      keyword = "link"
    when "automated"
      keyword = "cogs"
  end
  scroll_to_top_of_page
  job = @driver.find_elements(xpath: "//i[@class='fa fa-#{keyword}']/ancestor::tr/td[3]/a")
  pending("Required job is not present in the list of last few jobs.") if job.size == 0
  job.first.click
  @job_id = job.first.attribute("title")
  Selenium::WebDriver::Wait.new(timeout: 10).until{@job_id.include?(@driver.find_element(id: "reportTitle").text.split("ID ").last.split(" -").first)}
end


When /^I count the number of browser bubbles$/ do
  @bubbles = @driver.find_elements(css: "circle").size
  sleep 3
end

When /^I verify (\d+) number of browser bubbles$/ do |bubbles_count|
  @driver.find_elements(css: "circle").size == bubbles_count
  # @driver.find_element(xpath: "//a[@href='technology.html' and contains(text(),'Tech')] ").click
end

Then /^I verify last few jobs section is (open|closed)$/ do |action|
  dash = DashboardPage.new(@driver, @test_data)
  case action.downcase
    when "open"
      dash.dashboard_builds_section.displayed?.should == true
    when "closed"
      dash.dashboard_builds_section.displayed?.should == false
  end
end

Then /^I hit (restart|stop) button for latest (URL|AUTOMATED|LOAD) job$/ do |action,job_type|
  dash = DashboardPage.new(@driver,@test_data)
  case job_type.downcase
    when "url"
      keyword = "link"
    when "automated"
      keyword = "cogs"
    when "load"
      keyword = "truck"
  end
  if action.eql?"restart"
    out = dash.restart_job(keyword)
  else
    out = dash.stop_job(keyword)
  end
  pending("Required job is not present in the list of last few jobs. Please create one and then run the restart job test...") if out == "pending"
end

And /^I hit OK on stop job confirmation popup$/ do
  @driver.find_element(:xpath, "//button[contains(text(),'OK')]").click
  sleep(3)
end


And /^I see URL job status as stopped$/ do
  Selenium::WebDriver::Wait.new(timeout: 30).until{@driver.find_element(xpath: "//*[@id='lastfewjobs']/div/div[@class='table-responsive']/table/tbody/tr[1]/td[1]/a/i[6]")}.displayed?.should == true

 # @driver.find_element(:xpath, "//*[@id='lastfewjobs']/div/div[@class='table-responsive']/table/tbody/tr[1]/td[1]/a/i[6]").displayed?.should == true
end

When /^I follow consolidated report$/ do
  dash = DashboardPage.new(@driver,@test_data)
  dash.consolidated_report_button.click
  # sleep(3)
end

When /^I click ScaleNowReport button on dashboard page$/ do
  sleep(4)
  dash = DashboardPage.new(@driver,@test_data)
  dash.scalenow_report_button.click
end

Then /^I verify ScaleNow Report$/ do
  dash = DashboardPage.new(@driver,@test_data)
  dash.verifyScaleNowReport
end

When /^I search for (URL|AUTOMATED|SCALENOW) job id in dashboard$/ do |job_name|
  dash = DashboardPage.new(@driver,@test_data)
  dash.search_job_id(job_name)
end

Then /^I verify one row display with (URL|AUTOMATED|SCALENOW) job id$/ do |job_name|
  dash = DashboardPage.new(@driver,@test_data)
  case job_name.downcase
    when "url"
      keyword = "link"
    when "automated"
      keyword = "cogs"
    when "scalenow"
      keyword = "truck"
  end
  expect(@driver.find_element(xpath: "//i[@class='fa fa-#{keyword}']/ancestor::tr").text).to include($search_job_id)
  dash.search_textbox.clear
  sleep(2)
end