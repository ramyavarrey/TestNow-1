When /^I click TestNow button$/ do
  security = SecurityTestPage.new(@driver,@test_data)
  # securityTestPage.app_scan_click
  Selenium::WebDriver::Wait.new(timeout: 30).until{@driver.find_element(id: "runUrlTest")}.displayed?.should == true
  security.test_now_button_click
end

When /^I send Valid App URL$/ do
  securityTestPage = SecurityTestPage.new(@driver,@test_data)
  securityTestPage.application_url_text.clear
  securityTestPage.application_url_text.send_keys(@test_data['SECURITY_TEST_URL'])
  sleep 3
end


When /^I send space in AppUrl field on security test form$/ do
  securityTestPage = SecurityTestPage.new(@driver,@test_data)
  securityTestPage.application_url_text.clear
  securityTestPage.application_url_text.send_keys("asd")
end

And /^I do not select any security packs$/ do
  securityTestPage = SecurityTestPage.new(@driver,@test_data)
  securityTestPage.http_header_checkbox.click
end



And /^I checked (AppScan|HttpHeader) checkbox$/ do |value|
  securityTestPage = SecurityTestPage.new(@driver,@test_data)
  case value.downcase
    when "appscan"
      securityTestPage.app_scan_click
      sleep 3
    when "httpheader"
      securityTestPage.http_header_click
      sleep 3
  end
end

  Then /^I verify (AppScan|HttpHeader) packs$/ do |packs|
    securityTestPage = SecurityTestPage.new(@driver,@test_data)
    case packs.downcase
      when "appscanpacks"
        securityTestPage.app_scan_pack_label.text?.should == 'App Scan Pack'
        securityTestPage.app_scan_feature_heading.text?.should == 'Automated Application Security Scanning Run automated application level tests against the application using OWASP ZAP'
        securityTestPage.app_scan_feature1.text?.should == 'The application should not contain SQL injection vulnerabilities'
        securityTestPage.app_scan_feature2.text?.should == 'The application should not contain Cross Site Scripting vulnerabilities'
        securityTestPage.app_scan_feature3.text?.should == 'The application should not contain path traversal vulnerabilities'
        securityTestPage.app_scan_feature4.text?.should == 'The application should not contain remote file inclusion vulnerabilities'
        securityTestPage.app_scan_feature5.text?.should == 'The application should not contain Server side include vulnerabilities'
        securityTestPage.app_scan_feature6.text?.should == 'The application should not contain Server side code injection vulnerabilities'
        securityTestPage.app_scan_feature7.text?.should == 'The application should not contain Remote OS Command injection vulnerabilities'
        securityTestPage.app_scan_feature8.text?.should == 'The application should not contain CRLF injection vulnerabilities'
        securityTestPage.app_scan_feature9.text?.should == 'The application should not contain external redirect vulnerabilities'
        securityTestPage.app_scan_feature10.text?.should == 'The application should not disclose source code'
        securityTestPage.app_scan_feature11.text?.should == 'The application should not be vulnerable to Shell Shock'
        securityTestPage.app_scan_feature12.text?.should == 'The application should not be vulnerable to LDAP injection'
        securityTestPage.app_scan_feature13.text?.should == 'The application should not be vulnerable to XPATH injection'
        securityTestPage.app_scan_feature14.text?.should == 'The application should not be vulnerable to Xml External Entity Attacks'
        securityTestPage.app_scan_feature15.text?.should == 'The application should not be vulnerable to the Generic Padding Oracle attack'
        securityTestPage.app_scan_feature16.text?.should == 'The application should not expose insecure HTTP methods'
      when "httpheaderpacks"
        securityTestPage.http_header_pack_label.text?.should == 'HTTP Headers Pack'
        securityTestPage.http_header_feature_heading.text?.should == 'Http Headers Feature: Security settings on HTTP headers Verify that HTTP headers adequately protect data from attackers'
        securityTestPage.http_header_feature1.text?.should == 'Restrict other sites from placing it in an iframe in order to prevent ClickJacking attacks'
        securityTestPage.http_header_feature2.text?.should == 'Enable built in browser protection again Cross Site Scriping'
        securityTestPage.http_header_feature3.text?.should == 'Force the use of HTTPS for the base secure Url'
        securityTestPage.http_header_feature4.text?.should == 'Restrict HTML5 Cross Domain Requests to only trusted hosts'
        securityTestPage.http_header_feature5.text?.should == 'Enable anti-MIME sniffing prevention in browsers'
    end
  end

#When /^I submit the security test$/ do
 # security = SecurityTestPage.new(@driver,@test_data)
#  security.test_now_button.enabled?.should == true
#  security.test_now_button.click
#end

Then /^I verify Test Now button is disabled$/ do
  security = SecurityTestPage.new(@driver,@test_data)
  security.test_now_button.enabled?.should == false
end


