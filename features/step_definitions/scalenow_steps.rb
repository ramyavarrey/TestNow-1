And /^I fill in the scalenow test (SelectBrowser|Codebase|ExecutionDetails) form$/ do |form_name|
  scalenow = ScaleNowPage.new(@driver, @test_data)
  scalenow.fill_scalenow_form(form_name)
end

When /^I send space in (AppName|AppUrl|Parallel Users Count|Hours To Run|GitUrl|TestRunCmd) field on scalenow (SelectBrowser|Codebase|ExecutionDetails) form$/ do |field,form_name|
  scalenow = ScaleNowPage.new(@driver, @test_data)
  scalenow.fill_each_scalenow_field(field,form_name)
end

Then /^I verify error message for (AppName|AppUrl|Parallel Users Count|Hours To Run|GitUrl|TestRunCmd) field on scalenow form$/ do |field|
  scalenow = ScaleNowPage.new(@driver, @test_data)
  scalenow.verify_error_message(field)
end

Then /^I verify ScaleNow button is disabled$/ do
  scalenow = ScaleNowPage.new(@driver, @test_data)
  scalenow.scalenow_submit_button.enabled?.should == false
end

Then /^I submit the scalenow test job$/ do
  scalenow = ScaleNowPage.new(@driver, @test_data)
  scalenow.scalenow_submit_button.click
  sleep 2
end

Then /^I verify file uploded successfully$/ do

end




