When /^I fill in the automated test (SelectBrowser|Codebase|ExecutionDetails|ExecutionDetails with prescript) form$/ do |form_name|
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.fill_automated_test_form(form_name)
end

When /^I fill in the automated test (Codebase|ExecutionDetails) form with HP-UFT code$/ do |form_name|
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.fill_automated_test_form_hp_uft(form_name)

end


When /^I click (next|previous) button on (SelectBrowser|Codebase|ExecutionDetails) form$/ do |button_name, form_name|
  automated = AutomatedTestPage.new(@driver, @test_data)
  case button_name
    when "next"
      automated.click_next(form_name)
    when "previous"
      automated.click_previous(form_name)
    else
      p "** Invalid button used, please use only --> next <-- or --> previous <-- **"
  end
end

When /^I submit the automated test job$/ do
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.automated_test_submit_button.click
  sleep 2
end

When /^I click on sample (Ruby|Java|Appium|Rest API|HP-UFT) code link$/ do |sample_code|
  automated = AutomatedTestPage.new(@driver, @test_data)
  case sample_code.downcase
    when "ruby"
      automated.automated_ruby_code_link.click
    when "java"
      automated.automated_java_code_link.click
    when "appium"
      automated.automated_aapium_code_link.click
    when "rest api"
      automated.automated_restapi_code_link.click
    when "hp-uft"
      automated.automated_hp_uft_code_link.click
    else
      p "** Invalid language selected **"
  end
  sleep 2
end

Then /^I verify all data in summary table is as follows:$/ do |table|
  table.rows.each do |arr|
    @driver.find_element(xpath: "//td[contains(text(),'#{arr.first}')]/following-sibling::td").text.include?(arr.last).should == true
  end
end

When /^I send space in (AppName|AppUrl|GitUrl|TestRunCmd) field on automated test (SelectBrowser|Codebase|ExecutionDetails) form$/ do |field,form_name|
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.fill_each_automated_test_form(field,form_name)
end


Then /^I verify error message for (AppName|AppUrl|GitUrl|TestRunCmd) field$/ do |field|
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.check_error_message(field)
end


Then /^I verify (submit button|preserve VMs checkbox) is disable$/ do |item|
  automated = AutomatedTestPage.new(@driver,@test_data)
  case item
    when "submit button"
      automated.automated_test_submit_button.enabled?.should == false
    when "preserve VMs checkbox"
      automated.automated_preserve_vm_checkbox.selected?.should == false
      step "I verify all data in summary table is as follows:", table(%{
        | key                       | value |
        | Preserve machine if failed | false |
        })
  end
end

When /^I click on (SelectBrowser|SelectVersion|BaseUrl|Codebase|ExecutionDetails) of progress bar$/ do |progress_bar_name|
  automated = AutomatedTestPage.new(@driver, @test_data)
  case progress_bar_name
    when "SelectBrowser"
      automated.automated_test_progress_bar_select_browser.click
    when "SelectVersion"
      automated.automated_test_progress_bar_select_version.click
    when "BaseUrl"
      automated.automated_test_progress_bar_select_base_url.click
    when "Codebase"
      automated.automated_test_progress_bar_codebase.click
    when "ExecutionDetails"
      automated.automated_test_progress_execution_details.click
  end
  sleep 3
end

Then /^I verify I am on (SelectBrowser|Codebase|ExecutionDetails) form on (Load|Automated) Test page$/ do |form_name,page|
  if page == "Automated"
    automated = AutomatedTestPage.new(@driver, @test_data)
    automated.verify_automated_test_form(form_name)
  elsif page == "Load"
    load = ScaleNowPage.new(@driver, @test_data)
    load.verify_load_test_form(form_name)
  end
end

And /^I select OK button to submit job$/ do
  automated = AutomatedTestPage.new(@driver, @test_data)
  sleep(2)
  automated.automated_ruby_code_ok_button.click
  sleep(2)
end

And /^I select upload (automated test|scalenow|infra test) code from Codebase form$/ do |fields|
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.automated_upload_code.click
  automated.automated_upload_code_link.click
  sleep(2)
  # driver.switch_to.frame("myFrame")
  case fields
    when "automated test"
      file_path = File.absolute_path("testnowrubyexample-master.zip","data")
    when "scalenow"
      file_path = File.absolute_path("URLTest.zip","data")
    when "mobile app test"
      file_path = File.absolute_path("testnowappiumexample.zip","data")
    when "rest api test"
      file_path = File.absolute_path("testnowrestexample-master.zip","data")
    when "infra test"
      file_path = File.absolute_path("serverspec_verifynow.zip","data")
  end
  p file_path
  upload_file("file_up",file_path)
  sleep 3
  automated.automated_upload_button.click
  # driver.switch_to.default_content
  Selenium::WebDriver::Wait.new(timeout: 60).until {@driver.find_element(xpath: "//div[contains(text(),'Uploaded Successfully')]").displayed?}
  automated.automated_test_upload_file_close_button.click
  sleep(3)
end

When /^I select empty file in upload (automated test|infra test) code$/ do |fields|
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.automated_upload_code.click
  automated.automated_upload_code_link.click
  automated.automated_upload_button.click
  Selenium::WebDriver::Wait.new(timeout: 20).until {@driver.find_element(xpath: "//div[contains(text(),'File not uploaded properly. Please try again.')]").displayed?}
  automated.automated_test_upload_file_close_button.click
end

Then /^I verify zip file uploaded (successfully|fails)$/ do |items|
  case items
    when "successfully"
      expect(@driver.find_element(css: ".radio-inline>label:nth-of-type(1)").text).to include("Successfully Uploaded")
    when "fails"
      puts @driver.find_element(css: ".radio-inline>label:nth-of-type(2)").text
      expect(@driver.find_element(css: ".radio-inline>label:nth-of-type(2)").text).to include("Failed to Upload")
  end
  sleep(3)
end

When /^I add prescript in (mobile app|rest api) test form$/ do |form_name|
  automated = AutomatedTestPage.new(@driver, @test_data)
  case form_name
    when "mobile app"
      automated.fill_mobile_app_test_form
    when "rest api"
      automated.fill_rest_api_test_form
  end

  sleep(6)
end

Then /^I clear the data in Output Directory$/ do
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.automated_test_output_directory.clear
end

And /^I fill the Output Directory field$/ do
  automated = AutomatedTestPage.new(@driver, @test_data)
  automated.automated_test_output_directory.send_keys(@test_data['AUTOMATED_TEST_OUTPUT_DIRECTORY'])
end
