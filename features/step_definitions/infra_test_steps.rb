
And /^I select the Spec Type as (Serverspec|Awspec)$/ do  |spec_type|
  infratest = InfraTestPage.new(@driver, @test_data)

  case spec_type
    when "Serverspec"
      infratest.infra_test_serverspec_radiobutton.click
      sleep 2
      when "Awspec"
      infratest.infra_test_awspec_radiobutton.click
      sleep 2
  end

end


And /^I fill in the spec details with password$/ do
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.fill_infra_test_password_form
  sleep 2
end


And /^I fill in the spec details with key$/ do
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.fill_infra_test_key_form
  sleep 2
end

And /^I fill in the Awspec details form$/ do
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.fill_infra_test_awspec_form
  sleep 2
end


When /^I click (next|previous) button on (SpecDetails|ExecutionDetails) form$/ do |button_name, form_name|
  infratest = InfraTestPage.new(@driver, @test_data)
  case button_name
    when "next"
      infratest.click_next(form_name)
    when "previous"
      infratest.click_previous(form_name)
    else
      p "** Invalid button used, please use only --> next <-- or --> previous <-- **"
  end
end

When /^I fill in the infra test (Codebase|ExecutionDetails) form$/ do |form_name|
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.fill_infra_test_form(form_name)
end


Then /^I submit the infra test job$/ do
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.infra_test_submit_button.click
  sleep 2
end


When /^I send space in (Job Name|IP Address|user|password|key|GitUrl|TestRunCmd) field on infra test (SpecDetails|Codebase|ExecutionDetails) form$/ do |field,form_name|
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.fill_each_infra_test_form(field,form_name)
end


Then /^I verify error message for (Job Name|IP Address|user|password|key) field$/ do |field|
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.check_error_message(field)
end


And /^I select the Use Default Packages checkbox$/ do
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.select_default_packages
  sleep 2
end

And /^I select authentication type as key$/ do
  infratest = InfraTestPage.new(@driver, @test_data)
  infratest.infra_test_key_radiobutton.click
  sleep 2
end