Then /^I verify each links on help page$/ do
  help = HelpPage.new(@driver,@test_data)
  help.select_links_and_verify
end

Then /^I verify search text on help page$/ do
  help = HelpPage.new(@driver,@test_data)
  help.search_text_and_verify
end

Then /^I verify (all|each) faqs on help page$/ do |item|
  help = HelpPage.new(@driver,@test_data)
  help.verify_faqs(item)
end