Then /^I should see (profile|referral) section$/ do |section|
  profile = ProfilePage.new(@driver, @test_data)
  if section == "profile"
    profile.profile_section.displayed?.should == true
  elsif section == "referral"
    profile.referral_section.displayed?.should == true
  end
end

Then /^I should see profile details populated$/ do
  profile = ProfilePage.new(@driver,@test_data)
  Selenium::WebDriver::Wait.new(timeout: 30).until{@driver.find_element(xpath: "//label[contains(text(),'First Name')]/following-sibling::div/p").text == @test_data['TESTNOW_NAME']}
  #profile.verify_profile_data_displayed
end

Then /^I verify profile details to be (non-)?editable$/ do |action|
  profile = ProfilePage.new(@driver,@test_data)
  if action.nil?
    profile.verify_profile_data(:editable)
  else
    profile.verify_profile_data(:non_editable)
  end
end

Then /^I click on "([^"]*)" button on profile page$/ do |button|
  profile = ProfilePage.new(@driver,@test_data)
  case button.downcase
    when "edit profile"
      profile.profile_edit_button.click
    when "submit"

    else
      raise "Invalid Button"
  end
end

And /^I fill "([^"]*)" email in email id$/ do |item|
  profile = ProfilePage.new(@driver,@test_data)
  profile.refer_email_id.clear
  case item
    when "invalid"
      profile.refer_email_id.send_keys(Faker::Name.name)
    when "invalid@"
      profile.refer_email_id.send_keys(Faker::Name.name + "@")
    when "valid"
      profile.refer_email_id.send_keys(Faker::Internet.email)
  end
  profile.refer_button.click
  sleep 3
end

Then /^I verify error message for (invalid|valid) email id field$/ do |item|
  common = CommonPage.new(@driver,@test_data)
  case item
    when "invalid"
      expect(common.job_error_message.text).to include("Please enter valid email id")
    when "valid"
      expect(common.job_error_message.text).to include("Invitation is successfully sent to your friend")
  end

end