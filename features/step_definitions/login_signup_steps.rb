When /^I follow "([^"]*)" button$/ do |items|
  homepage = HomePage.new(@driver,@test_data)
   if items == "login"
     homepage.login_link.click
   elsif items == "signup"
    homepage.signup_button.click
  end
  sleep(3)
end

And /^I login to TestNow portal$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_in_login_form
  homepage.login_button.click
end


When /^I fill the signup form$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_in_signup_form
end


And /^I clicked Sign Up button$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.signup_submit_button.click
end

And /^I verify the user successfully logged in with email verification link$/ do
  homepage = HomePage.new(@driver,@test_data)
  Selenium::WebDriver::Wait.new(timeout: 30).until{@driver.find_element(xpath: "//a[contains(text(),'Click Here')]")}.displayed?.should == true
end


And /^I enter email address as (text|hello@gmail.|text@gmailcom|text@gmail|text@fdhghf)$/ do  |value|
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_in_invalid_email(value)

end



And /^I do not fill any of the details in the form$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.empty_aws_details
end

And /^I Click on Skip button$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.skip_button.click
  #element = driver.find_element(:xpath, "//div[@class='col-sm-12']")
 # element.location_once_scrolled_into_view
 # my_element = driver.find_element(:xpath, "//button[text()='Skip']")
 # my_element.click

end


And /^I hit OK on pop up message$/ do
  homepage = HomePage.new(@driver,@test_data)
  @driver.find_element(:xpath, "//button[contains(text(),'OK')]").click
  sleep(3)
end

And /^I fill in the AWS Details$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_aws_details
end

And /^I Click on Save button$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.save_button.click


end

Then /^I verify the successfull message of saving AWS Details$/ do
  homepage = HomePage.new(@driver,@test_data)
  #homepage.aws_success_message.text.should == "AWS Account for EC2 and S3 saved Successfully."
  Selenium::WebDriver::Wait.new(timeout: 30).until{@driver.find_element(xpath: "//div[@class='message ng-binding']")}.displayed?.should == true
end


And /^I login to TestNow portal with invalid credentials$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_in_invalid_login_form
  homepage.login_button.click
end


Then /^I verify error message on the TestNow page$/ do
  @driver.find_element(xpath: "//form[@name='loginForm']/div[1]").text.include?("Email Id is not registered.").should == true
end

And /^I login to TestNow portal with index$/ do
  index = ENV['RUN_INDEX']
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_in_login_form_for_index(index)
  homepage.login_button.click
end

Then /^I verify successful (login|logout) (?:to|from) TestNow portal$/ do |action|
  if action == "login"
    common = CommonPage.new(driver,@test_data)
    common.header_dashboard_link.displayed?.should == true
  elsif action == "logout"
    homepage = HomePage.new(@driver,@test_data)
    homepage.signup_button.displayed?.should == true
  end
end

And /^I logout form testnow portal$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.user_dropdown.click
  homepage.logout_button.click
end

And /^I signup with a current user$/ do
  home = HomePage.new(@driver,@test_data)
  home.fill_in_already_registered_signup_form
  home.signup_submit_button.click
end

And /^I signup with a invalid "([^"]*)" password$/ do |type|
  home = HomePage.new(@driver,@test_data)
  home.fill_in_signup_invalid_form(type)
  home.signup_submit_button.click
  sleep(3)
end

Then /^I verify "([^"]*)" password error$/ do |type|
  if type == "less digit"
    @driver.find_element(xpath: "//form[@name='signupForm']/div[4]").text.include?("Password cannot be less than 6 characters.").should == true
  elsif type == "more digit"
    @driver.find_element(xpath: "//form[@name='signupForm']/div[4]").text.include?("Password cannot be more than 10 characters").should == true
  elsif type == "alphanumeric"
    @driver.find_element(xpath: "//form[@name='signupForm']/div[4]/p[4]").text.include?("Password requires a mix of upper and lower case characters and at least one number.").should == true
  end

end

Then /^I verify email is already registered with user$/ do
  @driver.find_element(xpath: "//form[@name='signupForm']/div[@class='alert alert-danger ng-binding']").text.include?("User with this email is already registered.").should == true
end

And /^I left the fields empty$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.empty_signup_form
  end

Then /^I Verify the disabled Sign Up button$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.signup_submit_button.enabled? == false
end

And /^I enter all the details without selecting the checkbox$/ do
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_in_without_termsandconditions_signup_form

end

When /^I send (space) in (Name|Email|Password|Organization) field on Signup form$/ do |value,field|
  homepage = HomePage.new(@driver,@test_data)
  homepage.fill_in_individual_field(value,field)
end

Then /^I verify error message for (Name|Email|Password|Organization) field on Signup page$/ do |field|
  homepage = HomePage.new(@driver,@test_data)
  case field
    when "Name"
      homepage.signup_name_error_message.text.should == "Name can not be empty."
    when "Email"
      homepage.signup_email_error_message.text.should == "Please enter valid email."
    when "Password"
      homepage.signup_password_error_message.text.should == "Password can not be empty."
    when "Organization"
      homepage.signup_organization_error_message.text.should == "Organization can not be empty."
  end
end


