And /^I fill in the url test form$/ do
  url_test = URLTestPage.new(driver,@test_data)
  url_test.fill_url_test_form
end


And /^I fill in the url test form by selecting execution strategy as efficient$/ do
  url_test = URLTestPage.new(driver,@test_data)
  url_test.fill_url_test_Efficient_form
end


When /^I submit the url test$/ do
  url_test = URLTestPage.new(driver,@test_data)
  url_test.url_submit_test.click
  sleep(1)
end

Then /^I wait for job to be submitted with successful message$/ do
 # Selenium::WebDriver::Wait.new(timeout: 20).until {common.job_submission_message.displayed?}.should == true
  Selenium::WebDriver::Wait.new(timeout: 30).until{@driver.find_element(xpath: "//div[@id='jobSuccess']/a")}.displayed?.should == true
end

When /^I send (space|alphabet|negative) in (AppUrl|TextToSearch|Timeout) field on url test form$/ do |value,field|
  url = URLTestPage.new(@driver,@test_data)
  url.fill_individual_field(value,field)
end

Then /^I verify error message for (AppUrl|TextToSearch|Timeout) field on URL Test page$/ do |field|
  url = URLTestPage.new(@driver,@test_data)
  case field
    when "AppUrl"
      url.url_test_url_error_message.text.should == "Please enter valid application url."
    when "TextToSearch"
      url.url_text_to_search_error_message.text.should == "Please enter text to search in page."
    when "Timeout"
      url.url_timeout_error_message.text.should == "Please enter page load time out between 0 to 600."
  end
end

Then /^I verify TestNow button is disabled$/ do
  url = URLTestPage.new(@driver,@test_data)
  url.url_submit_test.enabled?.should == false
end

And /^I checked crawl url checkbox$/ do
  url = URLTestPage.new(@driver,@test_data)
  url.url_crawl_url_button.click
end



