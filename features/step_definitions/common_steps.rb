Given /^I am on TestNow (?:landing|home) page$/ do
  home = HomePage.new(@driver,@test_data)
  home.testnow_base_url
end

When /^I select browsers as follows:$/ do |table|
  $bv_map = Hash.new
  url_test = URLTestPage.new(@driver,@test_data)
  browser_hash = table.hashes.first
  browser_hash.each do |browser,version|
    url_test.select_browser_version(browser,version)
  end
end

When /^I navigate to "([^"]*)" (?:tab|item) from (?:header|footer)$/ do |tab_link|
  common = CommonPage.new(@driver,@test_data)
  common.navigation(tab_link)
  sleep 3
end

When /^I select "([^"]*)" from Automated tests dropdown$/ do |dropdown_list|
  common = CommonPage.new(@driver,@test_data)
  common.select_dropdown_automated_tests_list(dropdown_list)
end

When /^I select "([^"]*)" from Infra tests dropdown$/ do |dropdown_list|
  common = CommonPage.new(@driver,@test_data)
  common.select_dropdown_automated_tests_list(dropdown_list)
end

Then /^I should be on the "([^"]*)" page$/ do |target_page|
  common = CommonPage.new(@driver,@test_data)
  common.page_verification(target_page)
end

When /^I wait for (?:process|browser|job) to be submitted$/ do
  common = CommonPage.new(@driver,@test_data)
  Selenium::WebDriver::Wait.new(timeout: 20).until {common.job_submission_message.displayed?}
end

Then /^I wait for (URL|Automated|Security|UPA|Scale) job to complete with (\d+) browser$/ do |job_type,browser_count|
  dash = DashboardPage.new(@driver,@test_data)
  dash.wait_for_job_to_complete(job_type, browser_count)
end



Then /^I wait for (SelectBrowsers|OutputDirectory) error message to be displayed$/ do |error|
  common = CommonPage.new(@driver, @test_data)
  case error
    when "SelectBrowsers"
      common.job_error_message.text.include?("Please select browsers").should == true
    when "OutputDirectory"
      common.job_error_message.text.include?("Output Directory cannot be empty.").should == true
  end

end

When /^I follow take-a-tour link from (in|out)side$/ do |location|
  sleep(1)
  common = CommonPage.new(@driver,@test_data)
  Selenium::WebDriver::Wait.new(timeout: 60).until {common.footer_tnc_link.displayed?} if location == "in"
  common.footer_take_a_tour_link.click
end

Then /^I verify take-a-tour on (dashboard|report|login) page using (start-end|end-start) approach$/ do |page,approach|
  common = CommonPage.new(@driver,@test_data)
  common.traverse_through_take_a_tour(page, approach)
  sleep(2)
end

Then /^I end-tour and verify tour has ended$/ do
  common = CommonPage.new(@driver, @test_data)
  common.end_tour_and_verify
end

Then /^I wait for (EnterValidURL) message to be displayed$/ do |message|
  common = CommonPage.new(@driver, @test_data)
  case message
    when "EnterValidURL"
      common = CommonPage.new(@driver,@test_data)
      common.help_block_message.text.include?("Please enter valid application url.").should == true
  end
end