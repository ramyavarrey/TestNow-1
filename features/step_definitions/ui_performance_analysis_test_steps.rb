When /^I send (space|alphabet|negative) in (AppUrl|TextToSearch|Timeout) field on UPA test form$/ do |value,field|
  url = URLTestPage.new(@driver,@test_data)
  url.fill_individual_field(value,field)
end

Then /^I verify error message for (AppUrl|TextToSearch|Timeout) field on UPA Test page$/ do |field|
  url = URLTestPage.new(@driver,@test_data)
  case field
    when "AppUrl"
      url.url_test_url_error_message.text.should == "Please enter valid application url."
    when "TextToSearch"
      url.url_text_to_search_error_message.text.should == "Please enter text to search in page."
    when "Timeout"
      url.url_timeout_error_message.text.should == "Please enter page load time out between 0 to 600."
  end
end

Given /^I fill in the UPA test form$/ do
  upa_test = UpaTestPage.new(@driver, @test_data)
  upa_test.fill_upa_test_form
end

When /^I submit the UPA test$/ do
  upa_test = UpaTestPage.new(driver,@test_data)
  upa_test.test_now_button.click
  sleep(1)
end

And /^I fill in the UPA test form by selecting execution strategy as boost/ do
  upa_test = UpaTestPage.new(driver,@test_data)
  upa_test.fill_upa_test_boost_form
end


