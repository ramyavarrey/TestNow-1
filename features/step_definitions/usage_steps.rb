Then /^I verify (automated|manual) usage time column is sorted in "(desc|asc)" order$/ do |job_type,order|
  case job_type
    when "automated"
      keyword = "Details"
    when "manual"
      keyword = "Manual"
  end
  usage_time_col = @driver.find_elements(css: "tr[ng-repeat$=#{keyword}]>td:nth-of-type(4)")
  actual_usage_timings = []
  usage_time_col.each do |val|
    actual_usage_timings << val.text.gsub(" ","").to_i
  end

  if order.downcase == "asc"
    expected_usage_timings = actual_usage_timings.sort{|x,y| x <=> y}
    expect(actual_usage_timings).to eq(expected_usage_timings)
  elsif order.downcase == "desc"
    expected_usage_timings = actual_usage_timings.sort{|x,y| y <=> x}
    expect(actual_usage_timings).to eq(expected_usage_timings)
  else
    p "Invalid order"
  end
end


And /^I search for job id in run number column$/ do
  usage = UsagePage.new(@driver, @test_data)
  usage.usage_run_number_search_box.clear
  p "-----------Job ID is:#{@job_id}-----------"
  usage.usage_run_number_search_box.send_keys(@job_id)
  sleep 2
end

Then /^I verify usage statistic as per (AUTOMATED|URL) test job in dashboard$/ do |job_type|
  usage = UsagePage.new(@driver, @test_data)
  usage.usage_time.text.to_i.should == @bubbles
  usage.usage_job_type.text.should == job_type
end


And /^I click on the (automated|manual) tests usage time column header$/ do |job_type|
  usage = UsagePage.new(@driver, @test_data)
  sleep 3
  driver.execute_script("window.scrollTo(0,9999);")
  sleep 3
  case job_type
    when "automated"
      usage.automated_usage_time_col.click
    when "manual"
      usage.manual_usage_time_col.click
  end
  sleep 3
end

And /^I click on manual tests tab of manual tests page$/ do
  usage = UsagePage.new(@driver, @test_data)
  usage.manual_tests_tab.click
end

Then /^I verify "([^"]*)" title on usage page$/ do |header|
  @driver.find_element(tag_name: "h1").text.include?(header).should == true
end

Then /^I verify all sections of usage page are displayed$/ do
  usage = UsagePage.new(@driver,@test_data)
  expect(usage.usage_consume_rate.text).to include("Usage Time Consume Rate")
  expect(usage.usage_various_application.text).to include("Usage Time for Various Applications")
  expect(usage.usage_manual_testing.text).to include("Usage Time for Manual Testing")
  expect(usage.usage_details_header).to be_a Selenium::WebDriver::Element
end

And /^I select job type dropdown on usage page$/ do
  @job_type = select_random_value_from_dropdown_using_xpath("//select[@type='search']/option").gsub(" ","")
  p "---------job type is #{@job_type}---------"
  sleep(2)
end

Then /^I verify selected job type is display in job type column$/ do
  usage = UsagePage.new(@driver,@test_data)
  expect(usage.usage_job_type_col.text).to include(@job_type)
end
