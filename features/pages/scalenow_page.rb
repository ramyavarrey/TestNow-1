class ScaleNowPage < PageObject

  element :scalenow_app_name, {name: "appName"}
  element :scalenow_app_url, {name: "testURL"}
  element :scalenow_parallel_user_count, {id: "userCount"}
  element :scalenow_hours_to_run, {name: "runHours"}

  element :scalenow_git_url, {name: "gitURL"}
  element :scalenow_git_username, {name: "gitUser"}
  element :scalenow_git_password, {name: "gitPass"}
  element :scalenow_git_branch, {name: "branchName"}

  element :scalenow_prescript, {name: "preScript"}
  element :scalenow_test_run_command, {name: "commandToRunTest"}
  element :scalenow_output_directory, {xpath: "//label[contains(text(),'Directory')]/following-sibling::div/input"}
  element :scalenow_report_file, {xpath: "//label[contains(text(),'File')]/following-sibling::div/input"}
  element :scalenow_submit_button, {id: "submitLoadTestButton"}

  element :scalenow_app_name_error_message, {css: "input[name*='appName']~p"}
  element :scalenow_app_url_error_message, {css: "input[name*='testURL']~p"}
  element :scalenow_parallel_user_count_error_message, {css: "input[id*='userCount']~p"}
  element :scalenow_hours_to_run_error_message, {css: "input[name*='runHours']~p"}
  element :scalenow_git_url_error_message, {css: "input[name*='gitURL']~p"}
  element :scalenow_test_run_command_error_message, {css: "textarea[name*='commandToRunTest']~p"}

  element :scalenow_select_browser_form, {css: "#loadTestForm fieldset:nth-of-type(1)"}
  element :scalenow_code_base_form, {css: "#loadTestForm fieldset:nth-of-type(2)"}
  element :scalenow_execution_details_form, {css: "#loadTestForm fieldset:nth-of-type(3)"}


  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def fill_scalenow_form(form_name)
    wait = Selenium::WebDriver::Wait.new(timeout: 30)
    case form_name
      when "SelectBrowser"
        wait.until {scalenow_app_name.displayed?}
        scalenow_app_name.clear
        scalenow_app_name.send_keys(@test_data['SCALENOW_APP_NAME'])
        scalenow_app_url.clear
        scalenow_app_url.send_keys(@test_data['SCALENOW_APP_URL'])
        scalenow_parallel_user_count.clear
        scalenow_parallel_user_count.send_keys(@test_data['SCALENOW_PARALLEL_USER_COUNT'])
        scalenow_hours_to_run.clear
        scalenow_hours_to_run.send_keys(@test_data['SCALENOW_HOURS_TO_RUN'])

      when "Codebase"
        wait.until{scalenow_git_url.displayed?}
        scalenow_git_url.clear
        scalenow_git_url.send_keys(@test_data['SCALENOW_GIT_REPO'])
        scalenow_git_username.clear
        scalenow_git_username.send_keys(@test_data['SCALENOW_USER_ID'])
        scalenow_git_password.clear
        scalenow_git_password.send_keys(@test_data['SCALENOW_PASSWORD'])
        scalenow_git_branch.clear
        scalenow_git_branch.send_keys(@test_data['SCALENOW_GIT_BRANCH'])

      when "ExecutionDetails"
        wait.until {scalenow_prescript.displayed?}
        scalenow_prescript.clear
        scalenow_prescript.send_keys(@test_data['SCALENOW_PRESCRIPT'])
        scalenow_test_run_command.clear
        scalenow_test_run_command.send_keys(@test_data['SCALENOW_TEST_RUN_COMMAND'])
        scalenow_output_directory.clear
        scalenow_output_directory.send_keys(@test_data['SCALENOW_TEST_OUTPUT_DIRECTORY'])
        scalenow_report_file.clear
        scalenow_report_file.send_keys(@test_data['SCALENOW_TEST_REPORT_FILE'])
    end

  end

  def fill_each_scalenow_field(field,form_name)
    wait = Selenium::WebDriver::Wait.new(timeout: 30)
    case form_name
      when "SelectBrowser"
        wait.until {scalenow_app_name.displayed?}
        case field
          when "AppName"
            scalenow_app_name.clear
            scalenow_app_name.send_keys("abc")
            scalenow_app_name.clear
          when "AppUrl"
            scalenow_app_url.clear
            scalenow_app_url.send_keys("abc")
            scalenow_app_url.clear
          when "Parallel Users Count"
            scalenow_parallel_user_count.clear
            scalenow_parallel_user_count.send_keys('0')
          when "Hours To Run"
            scalenow_hours_to_run.clear
            scalenow_hours_to_run.send_keys('0')
        end

      when "Codebase"
        wait.until{scalenow_git_url.displayed?}
        scalenow_git_url.clear
        scalenow_git_url.send_keys('xyz')

      when "ExecutionDetails"
        wait.until {scalenow_test_run_command.displayed?}
        scalenow_test_run_command.clear
        scalenow_test_run_command.send_keys :space
    end
  end

  def verify_error_message(field)
    case field
      when "AppName"
        scalenow_app_name_error_message.text.include?('Please enter job name.').should == true
      when "AppUrl"
        scalenow_app_url_error_message.text.include?('Please enter valid application url.').should == true
      when "Parallel Users Count"
        scalenow_parallel_user_count_error_message.text.include?('Please enter valid Parallel VMs Count.').should == true
      when "Hours To Run"
        scalenow_hours_to_run_error_message.text.include?('Please enter valid Hours to Run.').should == true
      when "GitUrl"
        scalenow_git_url_error_message.text.include?('Please enter valid git url.').should == true
      when "TestRunCmd"
        scalenow_test_run_command_error_message.text.include?('Please enter command to run your test.').should == true
    end
  end

  def verify_load_test_form(form_name)
    case form_name
      when "SelectBrowser"
        scalenow_select_browser_form.displayed?.should == true
        scalenow_code_base_form.displayed?.should == false
        scalenow_execution_details_form.displayed?.should == false
      when "Codebase"
        scalenow_select_browser_form.displayed?.should == false
        scalenow_code_base_form.displayed?.should == true
        scalenow_execution_details_form.displayed?.should == false
      when "ExecutionDetails"
        scalenow_select_browser_form.displayed?.should == false
        scalenow_code_base_form.displayed?.should == false
        scalenow_execution_details_form.displayed?.should == true
    end
  end

end



