class HelpPage < PageObject

  element :help_search_textbox, {xpath: "//input[@type='search']"}


  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def select_links_and_verify
    links = @driver.find_elements(css: '#nav #list>li>a')
    links << @driver.find_elements(css: '#nav #list ul a').reverse
    links = links.flatten
    links.each do |link|
      id = link.attribute('ng-click').split("('").last.split("')").first
      link.click
      sleep 1
      expect(@driver.current_url).to include(id)
    end
  end

  def search_text_and_verify
    TestNow::HELP_TEXT_ARR .each do |help_text|
      help_search_textbox.clear
      help_search_textbox.send_keys(help_text)
      sleep 0.5
      search_text_arr = @driver.find_elements(css: "#nav #list li[class='']")
      expect(search_text_arr.size).to be >  0
      search_text_arr.each do |search_text|
        expect(search_text.text.downcase).to include(help_text.downcase)
      end
    end
  end

  def verify_faqs(item)
    faqs = @driver.find_elements(css: ".well div>a")
    faqs_desc = @driver.find_elements(css: ".well div>div")

    case item
      when "all"
        scroll_to_bottom_of_page    #added extra
        faqs.reverse.each_with_index do |faq,i|
          if(i==faqs.size-1)
            location = faq.location
            @driver.execute_script("window.scrollTo(#{location[0]},#{location[1]-100})")
          end
          expect(faqs_desc.reverse[i].displayed?).to be false
          faq.click
          sleep 0.5
          expect(faqs_desc.reverse[i].displayed?).to be true
        end
      when "each"
        scroll_to_bottom_of_page    #added extra
        faqs.reverse.each_with_index do |faq,i|
          expect(faqs_desc.reverse[i].displayed?).to be false
          # scroll_to_bottom_of_page
          if(i==faqs.size-1)
            location = faq.location
            @driver.execute_script("window.scrollTo(#{location[0]},#{location[1]-100})")
          end
          faq.click
          sleep 0.5
          expect(faqs_desc.reverse[i].displayed?).to be true
          faq.click
          sleep 0.5
          expect(faqs_desc.reverse[i].displayed?).to be false

        end
    end
  end
end