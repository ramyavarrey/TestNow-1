class CommonPage < PageObject

  element :header_dashboard_link, {id: "dashboard"}
  element :header_url_test_link, {id: "urltest"}
  element :header_automated_test_link, {id: "functionaltest"}
  element :header_manual_test_link, {id: "maualtest"}
  element :header_security_test_link, {id: "securitytest"}
  element :header_ui_perf_analysis_test_link, {id: "upatest"}
  element :header_scale_test_link, {id: "loadtest"}
  element :header_infra_link, {id: "infratest"}
  element :header_infra_test_link, {xpath: "//ul[@id='docdropdown']/li[1]/a[@id='urltest']"}


  element :header_docs_dropdown, {id: "docs"}
  element :docs_testnow_reference, {css: "#docs+ul a[href$='help']"}
  element :docs_configure_testnow, {css: "#docs+ul a[href$='configure_testnow']"}
  element :docs_faq, {css: "#docs+ul a[href*='community']"}

  element :header_profile_dropdown, {id: "userTab"}
  element :profile_dropdown_usage, {css: "#profiledropdown a[href*='usage']"}
  element :profile_dropdown_profile, {xpath: "//*[@id='profiledropdown']//a[text()='Profile']"}
  element :profile_dropdown_balance_hours, {css: "#profiledropdown a[href*='payment']"}

  element :header_quick_tests_dropdown, {id: "quicktest"}
  element :header_cross_browser_tab, {id: "crossbrowsertest"}
  element :header_mobile_app_tab, {id: "mobileapptest"}
  element :header_rest_api_tab, {id: "restapitest"}

  element :footer_powered_by_link, {xpath: "//i[contains(text(),'Powered by REAN Cloud')]"}
  element :footer_tnc_link, {partial_link_text: "Terms"}
  element :footer_jenkins_plugin_link, {partial_link_text: "Jenkins"}
  element :footer_help_link, {partial_link_text: "Help"}
  element :footer_take_a_tour_link, {id: "demo"}

  # element :job_submission_message, {css: "div.message.ng-binding"}
  element :job_submission_message, {xpath: "//div[@ng-show='showJobMessage']"}

  element :job_error_message, {css: "div.message.ng-binding"}
  element :tour_grayed_screen, {css: ".tour-backdrop"}
  element :end_tour_button, {css: "button[data-role*='end']"}
  element :help_block_message, {css: ".help-block"}

  def initialize(page_driver,test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def navigation(tab_link)
    sleep(1)
    case tab_link.downcase
      when "dashboard"
        header_dashboard_link.click
        sleep(2)
      when "url test"
        header_quick_tests_dropdown.click
        sleep(2)
        header_url_test_link.click
      when "manual test"
        header_quick_tests_dropdown.click
        sleep(2)
        header_manual_test_link.click
      when "security test"
        header_quick_tests_dropdown.click
        sleep(2)
        header_security_test_link.click
      when "ui performance analysis test"
        header_quick_tests_dropdown.click
        sleep(2)
        header_ui_perf_analysis_test_link.click
      when "automated test"
        header_automated_test_link.click
      when "cross browser"
        header_automated_test_link.click
        sleep(2)
        header_cross_browser_tab.click
      when "infra test"
        header_infra_link.click
        sleep(2)
        header_infra_test_link


      # when "mobile app"
      #   header_automated_test_link.click
      #   sleep(2)
      #   header_mobile_app_tab.click
     # when "rest api"
       # header_automated_test_link.click
       # sleep(2)
       # header_rest_api_tab.click
      when "scalenow"
        header_scale_test_link.click
      when "usage"
        header_profile_dropdown.click
        profile_dropdown_usage.click
      when "load test"
        header_scale_test_link.click
      when "balance hours"
        header_profile_dropdown.click
        profile_dropdown_balance_hours.click
      when "powered by"
        footer_powered_by_link.click
      when "terms & conditions"
        footer_tnc_link.click
        sleep 2
      when "jenkins plugin"
        footer_jenkins_plugin_link.click
      when "help"
        Selenium::WebDriver::Wait.new(timeout: 60).until {footer_tnc_link.displayed?}
        header_docs_dropdown.click
        docs_testnow_reference.click
      when "configure testnow"
        header_docs_dropdown.click
        docs_configure_testnow.click
      when "faqs"
        Selenium::WebDriver::Wait.new(timeout: 60).until {footer_tnc_link.displayed?}
        header_docs_dropdown.click
        docs_faq.click
      when "profile"
        header_profile_dropdown.click
        profile_dropdown_profile.click
      when "take a tour"
        footer_take_a_tour.click
      else
        fail("Invalid option or option not defined")
    end
  end


  def page_verification(target_page)
    sleep(1)
    wait = Selenium::WebDriver::Wait.new(timeout: 120)
    case target_page.downcase
      when "dashboard"
        wait.until {driver.current_url.include?("home")}
        @driver.find_element(xpath: "//button[contains(text(),'Consolidated Report')]").text.include?("Consolidated Report").should == true
      when "url test"
        wait.until {driver.current_url.include?(target_page.downcase.gsub(" ",""))}
        @driver.find_element(id: "urltestheading").text.should == target_page

      when "manual test"
        wait.until {driver.current_url.include?(target_page.downcase.gsub(" ",""))}
        @driver.find_element(id: "buttonStartNow").displayed?.should == true

      when "security test"
        wait.until {driver.current_url.include?(target_page.downcase.gsub(" ",""))}
        @driver.find_element(id: "urltestheading").text.should == target_page

      when "ui performance analysis test"
        wait.until {driver.current_url.include?("upatest")}
        @driver.find_element(id: "urltestheading").text.should == target_page

      when "automated test"
        wait.until {driver.current_url.include?("functionaltest")}
        wait.until {@driver.find_element(css: "img[src*='Safari']").displayed?}
        @driver.find_element(css: "#functionalTesTForm>label").text.should == "Automated Test"

      when "cross browser"
        wait.until {driver.current_url.include?("functionaltest")}
        wait.until {@driver.find_element(css: "img[src*='Safari']").displayed?}
        @driver.find_element(css: "#functionalTesTForm>label").text.should == "Automated Test"

      # when "mobile app"
      #   wait.until {driver.current_url.include?("mobileapptest")}
      #   # wait.until {@driver.find_element(css: "img[src*='Safari']").displayed?}
      #   @driver.find_element(css: "#functionalTesTForm>label").text.should == "Mobile App Test"

      when "rest api"
        wait.until {driver.current_url.include?("restapitest")}
        # wait.until {@driver.find_element(css: "img[src*='Safari']").displayed?}
        @driver.find_element(css: "#functionalTesTForm>label").text.should == "Rest API test"

      when "usage"
        wait.until {driver.current_url.include?(target_page.downcase)}
        @driver.find_element(css: "h1.page-header").text.should == "Usage Details for Last 30 days"

      when "load test"
        wait.until {driver.current_url.include?(target_page.downcase.gsub(" ",""))}
        @driver.find_element(name: "runHours").displayed?.should == true

      when "balance hours"
        wait.until {driver.current_url.include?("payment")}
        @driver.find_elements(css: "div.panel-heading h4").first.text.include?("PayForUse").should == true

      when "opex software"
        @driver.find_element(css: "img[src*=opex_logo]").displayed?.should == true
        @driver.navigate.back

      when "rean cloud"
        @driver.find_element(xpath: "//img[@class='reversed']").displayed?.should == true
        @driver.navigate.back

      when "terms & conditions"
        expect(@driver.find_element(xpath: "//h4[contains(text(),'Terms and Conditions')]").displayed?).to be true
        @driver.find_element(xpath: "//div[@id='tandc']/div/div/div[3]/button[contains(text(),'Close')]").click
      when "help jenkins plugin"
        wait.until {driver.current_url.include?("help#jenkins-plugin")}
        @driver.find_element(css: "div#jenkins-plugin>h2").text.should == "Jenkins Plugin"

      when "help"
        wait.until {driver.current_url.include?("help")}
        @driver.find_element(css: "div#introduction>h1").text.should == "Introduction"

      when "configure testnow"
        wait.until {@driver.current_url.include?("help#configure_testnow")}
        @driver.find_element(id: "configure_testnow").text.should == "Configure TestNow"

      when "faqs"
        wait.until {@driver.current_url.include?("help#community")}
        @driver.find_element(css: "#community>h3").text.should == "FAQs"

      when "profile"
        wait.until {@driver.current_url.include?("profile")}
        @driver.find_element(css: "button[ng-click*='editProfile']").displayed?.should == true

      else
        fail("Invalid option or option not defined")
    end
  end

  def traverse_through_take_a_tour(destination,approach)
    case destination
      when "dashboard"
        tat = TestNow::TAKE_A_TOUR_DASHBOARD
      when "report"
        tat = TestNow::TAKE_A_TOUR_REPORT
      when "login"
        tat = TestNow::TAKE_A_TOUR_LOGIN
      else
        tat = {}
        p "Invalid page"
    end
    reverse_tat = Hash[tat.to_a.reverse]
    case approach
      when "start-end"
        tat.each_with_index do |set,i|
          expect(tour_grayed_screen.displayed?).to be true
          expect(@driver.find_element(css: "div#step-#{set.first}>h3").text).to include(set.last)
          @driver.find_element(css: "div#step-#{set.first} button[data-role*='next']").send_keys(:arrow_right) if i != tat.size-1
          sleep(2)
        end
      when "end-start"
        reverse_tat.each_with_index do |set,i|
          expect(tour_grayed_screen.displayed?).to be true
          expect(@driver.find_element(css: "div#step-#{set.first}>h3").text).to include(set.last)
          @driver.find_element(css: "div#step-#{set.first} button[data-role*='next']").send_keys(:arrow_left) if i != reverse_tat.size-1
          sleep(1)
        end
    end
  end

  def end_tour_and_verify
    end_tour_button.click
    expect(@driver.find_elements(css: ".tour-backdrop").size).to eq(0)
  end

  def select_dropdown_automated_tests_list(dropdown_list)
    case dropdown_list.downcase
      when "cross browser"
        header_cross_browser_tab.click
        sleep 5
      when "test"
        header_infra_test_link.click
        sleep(2)


      when "mobile app"
        header_mobile_app_tab.click
      when "rest api"
        header_rest_api_tab.click
    end
  end

end
