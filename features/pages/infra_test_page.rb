class InfraTestPage < PageObject

  element :infra_test_serverspec_radiobutton,  {xpath: "//input[@value='serverspec']"}
  element :infra_test_awspec_radiobutton,  {xpath: "//input[@value='awspec']"}

  element :infra_test_job_name,   {xpath: "//input[@name='appName']"}


  element :infra_test_ip_address,   {xpath: "//input[@name='infraTestIps']"}
  element :infra_test_user,   {xpath: "//input[@name='infraTestUser']"}
  element :infra_test_password_radiobutton,  {xpath: "//input[@value='password']"}
  element :infra_test_key_radiobutton,  {xpath: "//input[@value='key']"}

  element :infra_test_password,   {xpath: "//input[@name='infraTestPassword']"}
  element :infra_test_key,   {xpath: "//textarea[@name='infraTestKey']"}

  element :infra_test_access_key,   {xpath: "//input[@name='infraTestAccessKey']"}
  element :infra_test_secret_key,   {xpath: "//input[@name='infraTestSecreteKey']"}
  element :infra_test_region,   {xpath: "//input[@name='infraTestAWSRegion']"}
  element :infra_test_git_url, {name: "gitURL"}
  element :infra_test_git_branch, {name: "branchName"}
  element :infra_test_run_command, {name: "commandToRunTest"}
  element :infra_test_output_directory, {xpath: "//label[contains(text(),'Directory')]/following-sibling::div/input"}
  element :infra_test_report_file, {xpath: "//label[contains(text(),'File')]/following-sibling::div/input"}

  element :infra_test_next_button_spec_details,   {xpath: "//fieldset[1]//input[@id='nextButton']"}
  element :infra_test_next_button_codebase_details,   {xpath: "//fieldset[2]//input[@id='nextButton']"}
  element :infra_test_submit_button, {name: "submit"}

  element :infra_test_default_package_checkbox, {xpath: "//div[@class='col-sm-1']/label/input"}
  element :infra_test_linux_security_checkbox, {xpath: "//div[@class='col-sm-8']/label[1]/input"}
  element :infra_test_cis_audit_checkbox, {xpath: "//div[@class='col-sm-8']/label[2]/input"}
  element :infra_test_base_security_checkbox, {xpath: "//div[@class='col-sm-8']/label[3]/input"}
  element :infra_test_security_audit_checkbox, {xpath: "//div[@class='col-sm-8']/label[4]/input"}


  element :infra_test_job_name_error_message, {css: "input[name*='appName']~p"}
  element :infra_test_ip_address_error_message, {css: "input[name*='infraTestIps']~p"}
  element :infra_test_user_error_message, {css: "input[name*='infraTestIps']~p"}

  element :infra_test_password_error_message, {css: "input[name*='infraTestPassword']~p"}
  element :infra_test_key_error_message, {css: "input[name*='infraTestKey']~p"}

  element :infra_test_git_url_error_message, {css: "input[name*='gitURL']~p"}
  element :infra_test_run_command_error_message, {css: "textarea[name*='commandToRunTest']~p"}








  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def fill_infra_test_awspec_form


    infra_test_job_name.clear
    infra_test_job_name.send_keys(@test_data['INFRA_TEST_JOB_NAME'])
    infra_test_access_key.clear
    infra_test_access_key.send_keys(@test_data['INFRA_TEST_ACCESS_KEY'])
    infra_test_secret_key.clear
    infra_test_secret_key.send_keys(@test_data['INFRA_TEST_SECRET_KEY'])
    infra_test_region.clear
    infra_test_region.send_keys(@test_data['INFRA_TEST_REGION'])


  end

  def fill_infra_test_password_form

        infra_test_job_name.clear
    infra_test_job_name.send_keys(@test_data['INFRA_TEST_JOB_NAME'])
    infra_test_ip_address.clear
    infra_test_ip_address.send_keys(@test_data['INFRA_TEST_IP_ADDRESS'])
    infra_test_user.clear
    infra_test_user.send_keys(@test_data['INFRA_TEST_USER'])
    infra_test_password_radiobutton.click
    infra_test_password.clear
    infra_test_password.send_keys(@test_data['INFRA_TEST_PASSWORD'])
  end


  def fill_infra_test_key_form


        infra_test_job_name.clear
    infra_test_job_name.send_keys(@test_data['INFRA_TEST_JOB_NAME'])
    infra_test_ip_address.clear
    infra_test_ip_address.send_keys(@test_data['INFRA_TEST_IP_ADDRESS'])
    infra_test_user.clear
    infra_test_user.send_keys(@test_data['INFRA_TEST_USER'])
    infra_test_key_radiobutton.click
    infra_test_key.clear
    infra_test_key.send_keys(@test_data['INFRA_TEST_KEY'])

  end


  def fill_infra_test_form(form_name)
    wait = Selenium::WebDriver::Wait.new(timeout: 30)
    case form_name


      when "Codebase"
        wait.until{infra_test_git_url.displayed?}
        infra_test_git_url.clear
        infra_test_git_url.send_keys(@test_data['INFRA_TEST_GIT_REPO'])
        infra_test_git_branch.clear
        infra_test_git_branch.send_keys(@test_data['INFRA_TEST_GIT_BRANCH'])

      when "ExecutionDetails"
        wait.until {infra_test_run_command.displayed?}
        infra_test_run_command.clear
        infra_test_run_command.send_keys(@test_data['INFRA_TEST_RUN_COMMAND'])
        infra_test_output_directory.clear
        infra_test_output_directory.send_keys(@test_data['INFRA_TEST_OUTPUT_DIRECTORY'])
        infra_test_report_file.clear
        infra_test_report_file.send_keys(@test_data['INFRA_TEST_REPORT_FILE'])
    end
  end



  def click_next(form_name)
    case form_name
      when "SpecDetails"
        infra_test_next_button_spec_details.click
        sleep(2)
      when "Codebase"
        infra_test_next_button_codebase_details.click
        sleep(2)
    end
  end

  def fill_each_infra_test_form(field,form_name)
    wait = Selenium::WebDriver::Wait.new(timeout: 30)
    case form_name
      when "SpecDetails"
        wait.until {infra_test_job_name.displayed?}
        case field
          when "JobName"
            infra_test_job_name.clear
            infra_test_job_name.send_keys("abc")
            infra_test_job_name.clear
          when "IP Address"
            infra_test_ip_address.clear
            infra_test_ip_address.send_keys("abc")
            infra_test_ip_address.clear
          when "user"
            infra_test_user.clear
            infra_test_user.send_keys("abc")
            infra_test_user.clear
          when "password"
            infra_test_password.clear
            infra_test_password.send_keys("abc")
            infra_test_password.clear
          when "key"
            infra_test_key.clear
            infra_test_key.send_keys("abc")
            infra_test_key.clear
        end

      when "Codebase"
        wait.until{infra_test_git_url.displayed?}
        infra_test_git_url.clear
        infra_test_git_url.send_keys('xyz')

      when "ExecutionDetails"
        wait.until {infra_test_run_command.displayed?}
        infra_test_run_command.clear
        infra_test_run_command.send_keys("abc")
        infra_test_run_command.clear
    end
  end

  def check_error_message(field)
    case field
      when "Job Name"
        infra_test_job_name_error_message.text.include?("Please enter job name.").should == true
      when "IP Address"
        infra_test_ip_address_error_message.text.include?("Please enter valid application url.").should == true
      when "user"
        infra_test_user_error_message.text.include?("Please enter valid user.").should == true
      when "password"
        infra_test_password_error_message.text.include?("Please enter valid password.").should == true
      when "key"
        infra_test_key_error_message.text.include?("Please enter valid key.").should == true
      when "GitUrl"
        infra_test_git_url_error_message.text.include?("Please enter valid git url.").should == true
      when "TestRunCmd"
        infra_test_run_command_error_message.text.include?("Please enter command to run your test.").should == true
    end
  end

  def select_default_packages


    infra_test_default_package_checkbox.click

    infra_test_linux_security_checkbox.click

    infra_test_cis_audit_checkbox.click

    infra_test_base_security_checkbox.click

    infra_test_security_audit_checkbox.click

  end


end




