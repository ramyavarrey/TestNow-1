class UsagePage < PageObject

  element :usage_run_number_search_box, {xpath: "//input[@st-search='jobId']"}
  element :usage_time, {xpath: "//td[contains(text(),'#{@job_id}')]/ancestor::tr/td[4]"}
  element :usage_job_type, {xpath: "//td[contains(text(),'#{@job_id}')]/ancestor::tr/td[2]"}
  element :automated_usage_time_col, {xpath: "//th[contains(text(),'Run Number')]/ancestor::table/thead/tr/th[contains(text(),'Usage')]"}

  element :usage_consume_rate, {id: "bar-chart"}
  element :usage_various_application, {xpath: "//div[@id='pie-chart'][1]"}
  element :usage_manual_testing, {xpath: "//div[@id='pie-chart'][2]"}
  element :usage_details_header, {xpath: "//div[contains(text(),'Usage Details')]"}

  element :manual_tests_tab, {xpath: "//a[contains(text(),'Manual Tests')]"}
  element :manual_usage_time_col, {xpath: "//table[@st-table='usageDetailsManual']/thead/tr/th[contains(text(),'Usage Time')]"}

  element :usage_job_type_col, {xpath: "//table[@st-table='usageDetails']/tbody/tr[1]/td[2]"}

  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

end