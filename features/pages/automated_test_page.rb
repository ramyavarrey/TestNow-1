class AutomatedTestPage < PageObject

  element :automated_test_app_name, {name: "appName"}
  element :automated_test_app_url, {name: "testURL"}
  element :automated_uft_radiobutton, {xpath: "//input[@value='UFT']"}
  element :automated_test_git_url, {name: "gitURL"}
  element :automated_test_git_branch, {name: "branchName"}
  element :automated_test_run_command, {name: "commandToRunTest"}
  element :automated_test_output_directory, {xpath: "//label[contains(text(),'Directory')]/following-sibling::div/input"}
  element :automated_test_report_file, {xpath: "//label[contains(text(),'File')]/following-sibling::div/input"}
  element :automated_test_prescript, {name: "preScript"}
  element :automated_preserve_vm_checkbox, {css: "div.checkbox input"}

  element :automated_test_next_button_browser_form, {xpath:"//fieldset[1]//input[@id='nextButton']"}
  element :automated_test_next_button_codebase_form, {xpath:"//fieldset[2]//input[@id='nextButton']"}
  element :automated_test_previous_button_codebase_form, {xpath:"//fieldset[2]//input[@id='previousButton']"}
  element :automated_test_previous_button_execution_details_form, {xpath:"//fieldset[3]//input[@id='previousButton']"}
  element :automated_test_submit_button, {name: "submit"}

  element :automated_test_progress_bar_select_browser, {xpath: "//ul[@id='progressbar']/li[contains(text(),'Select Browser')]"}
  element :automated_test_progress_bar_select_version, {xpath: "//ul[@id='progressbar']/li[contains(text(),'Select Version')]"}
  element :automated_test_progress_bar_select_base_url, {xpath: "//ul[@id='progressbar']/li[contains(text(),'Base Url')]"}
  element :automated_test_progress_bar_codebase, {xpath: "//ul[@id='progressbar']/li[contains(text(),'Codebase')]"}
  element :automated_test_progress_execution_details, {xpath: "//ul[@id='progressbar']/li[contains(text(),'Execution Details')]"}

  element :automated_ruby_code_link, {link: "Ruby"}
  element :automated_ruby_code_ok_button, {xpath: "//button[contains(text(),'OK')]"}
  element :automated_java_code_link, {link: "Java"}
  element :automated_hp_uft_code_link, {link: "HP-UFT"}
  element :automated_aapium_code_link, {link: "Appium"}
  element :automated_restapi_code_link, {link: "RestAPI"}

  element :automated_upload_code, {id: "chkLink"}
  element :automated_upload_code_link, {link: "Click to upload"}
  element :automated_select_file_button, {xpath: "//div[@class='fileUpload btn btn-primary']"}
  element :automated_upload_button, {xpath: "//div/span[contains(text(),'Upload Zip File')]"}
  element :automated_test_upload_file_close_button, {css: "div#uploadCodeModel div.modal-footer>button"}

  element :automated_test_app_name_error_message, {css: "input[name*='appName']~p"}
  element :automated_test_app_url_error_message, {css: "input[name*='testURL']~p"}
  element :automated_test_git_url_error_message, {css: "input[name*='gitURL']~p"}
  element :automated_test_run_command_error_message, {css: "textarea[name*='commandToRunTest']~p"}

  element :automated_select_browser_form, {css: "#functionalTesTForm fieldset:nth-of-type(1)"}
  element :automated_code_base_form, {css: "#functionalTesTForm fieldset:nth-of-type(2)"}
  element :automated_execution_details_form, {css: "#functionalTesTForm fieldset:nth-of-type(3)"}


  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def fill_automated_test_form(form_name)
    wait = Selenium::WebDriver::Wait.new(timeout: 30)
    case form_name
      when "SelectBrowser"
        wait.until {automated_test_app_name.displayed?}
        automated_test_app_name.clear
        automated_test_app_name.send_keys(@test_data['AUTOMATED_TEST_APP_NAME'])
        automated_test_app_url.clear
        automated_test_app_url.send_keys(@test_data['AUTOMATED_TEST_APP_URL'])

      when "Codebase"
        wait.until{automated_test_git_url.displayed?}
        automated_test_git_url.clear
        automated_test_git_url.send_keys(@test_data['AUTOMATED_TEST_GIT_REPO'])
        automated_test_git_branch.clear
        automated_test_git_branch.send_keys(@test_data['AUTOMATED_TEST_GIT_BRANCH'])

      when "ExecutionDetails"
        wait.until {automated_test_run_command.displayed?}
        automated_test_run_command.clear
        automated_test_run_command.send_keys(@test_data['AUTOMATED_TEST_RUN_COMMAND'])
        automated_test_output_directory.clear
        automated_test_output_directory.send_keys(@test_data['AUTOMATED_TEST_OUTPUT_DIRECTORY'])
        automated_test_report_file.clear
        automated_test_report_file.send_keys(@test_data['AUTOMATED_TEST_REPORT_FILE'])

      when "ExecutionDetails with prescript"

        wait.until {automated_test_run_command.displayed?}
        automated_test_prescript.clear
        automated_test_prescript.send_keys(@test_data['AUTOMATED_TEST_PRESCRIPT'])
        automated_test_run_command.clear
        automated_test_run_command.send_keys(@test_data['AUTOMATED_TEST_RUN_COMMAND'])
        automated_test_output_directory.clear
        automated_test_output_directory.send_keys("testnowrubyexample-master/reports")
        automated_test_report_file.clear
        automated_test_report_file.send_keys(@test_data['AUTOMATED_TEST_REPORT_FILE'])
    end

  end

  def fill_automated_test_form_hp_uft(form_name)
    case form_name
      when "Codebase"
        wait = Selenium::WebDriver::Wait.new(timeout: 30)
        wait.until{automated_test_git_url.displayed?}
        automated_uft_radiobutton.click
        automated_test_git_url.clear
        automated_test_git_url.send_keys(@test_data['AUTOMATED_TEST_GIT_REPO_UFT'])
        automated_test_git_branch.clear
        automated_test_git_branch.send_keys(@test_data['AUTOMATED_TEST_GIT_BRANCH'])

      when "ExecutionDetails"
        #wait.until {automated_test_run_command.displayed?}
        automated_test_run_command.clear
        automated_test_run_command.send_keys(@test_data['AUTOMATED_TEST_RUN_COMMAND_UFT'])
        automated_test_output_directory.clear
        automated_test_output_directory.send_keys(@test_data['AUTOMATED_TEST_OUTPUT_DIRECTORY'])
        automated_test_report_file.clear
        automated_test_report_file.send_keys(@test_data['AUTOMATED_TEST_REPORT_FILE_UFT'])

    end




  end

  def click_next(form_name)
    case form_name
      when "SelectBrowser"
        automated_test_next_button_browser_form.click
        sleep(2)
      when "Codebase"
        automated_test_next_button_codebase_form.click
        sleep(2)
    end
  end

  def click_previous(form_name)
    case form_name
      when "Codebase"
        automated_test_previous_button_codebase_form.click
        sleep(2)
      when "ExecutionDetails"
        automated_test_previous_button_execution_details_form.click
        sleep(2)
    end
  end


  def fill_each_automated_test_form(field,form_name)
    wait = Selenium::WebDriver::Wait.new(timeout: 30)
    case form_name
      when "SelectBrowser"
        wait.until {automated_test_app_name.displayed?}
        case field
          when "AppName"
            automated_test_app_name.clear
            automated_test_app_name.send_keys("abc")
            automated_test_app_name.clear
          when "AppUrl"
            automated_test_app_url.clear
            automated_test_app_url.send_keys("abc")
            automated_test_app_url.clear
        end

      when "Codebase"
        wait.until{automated_test_git_url.displayed?}
        automated_test_git_url.clear
        automated_test_git_url.send_keys('xyz')

      when "ExecutionDetails"
        wait.until {automated_test_run_command.displayed?}
        automated_test_run_command.clear
        automated_test_run_command.send_keys("abc")
        automated_test_run_command.clear
    end
  end

  def check_error_message(field)
    case field
      when "AppName"
        automated_test_app_name_error_message.text.include?("Please enter job name.").should == true
      when "AppUrl"
        automated_test_app_url_error_message.text.include?("Please enter valid application url.").should == true
      when "GitUrl"
        automated_test_git_url_error_message.text.include?("Please enter valid git url.").should == true
      when "TestRunCmd"
        automated_test_run_command_error_message.text.include?("Please enter command to run your test.").should == true
    end
  end

  def verify_automated_test_form(form_name)
    case form_name
      when "SelectBrowser"
        automated_select_browser_form.displayed?.should == true
        automated_code_base_form.displayed?.should == false
        automated_execution_details_form.displayed?.should == false
      when "Codebase"
        automated_select_browser_form.displayed?.should == false
        automated_code_base_form.displayed?.should == true
        automated_execution_details_form.displayed?.should == false
      when "ExecutionDetails"
        automated_select_browser_form.displayed?.should == false
        automated_code_base_form.displayed?.should == false
        automated_execution_details_form.displayed?.should == true
    end
  end

  def fill_mobile_app_test_form
    automated_test_prescript.clear
    automated_test_prescript.send_keys("cd testnowappiumexample\ncd AppiumProject\nmvn clean install -DskipTests\ndevice=`adb devices | grep \"emulator\" | awk '{print $1;}'`\nexport DEVICENAME=$device\necho \"Device is: ${device} .............................\"\necho \"Version is ${VERSION}\"")
    sleep(2)
    automated_test_output_directory.clear
    automated_test_output_directory.send_keys("testnowappiumexample/AppiumProject/target/surefire-reports")
  end

  def fill_rest_api_test_form
    automated_test_prescript.clear
    automated_test_prescript.send_keys("cd testnowrestexample-master\nclean install")
    sleep(2)
    automated_test_output_directory.clear
    automated_test_output_directory.send_keys("testnowrestexample-master/target/surefire-reports")

  end

end
