class ManualTestPage < PageObject

  element :manual_name, {name: "appName"}
  element :manual_resolution_select, {css: "div>select"}
  element :manual_start_now_button, {id: "buttonStartNow"}

  element :manual_delete_button, {xpath: "//button[contains(text(),'Delete')]"}
  element :manual_restore_button, {xpath: "//button[contains(text(),'Restore')]"}
  element :manual_sleep_button, {xpath: "//button[contains(text(),'Sleep')]"}

  element :manual_automation_test_vms_tab, {link: "Automation test VM(s)"}

  element :manual_test_job_submission_message, {xpath: "//div[contains(text(),'successfully')]"}
  element :manual_test_job_process_message, {css: "div.message.ng-binding"}


  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def fill_manual_test_form(values)
    manual_name.clear
   if values == "nil"
        manual_name.send_keys(" ")
   else
     manual_name.send_keys(@test_data['MANUAL_TEST_NAME'])
    end
    sleep 2
  end


  def select_resolution(resolution)
    option_selected = select_random_value_from_dropdown_using_xpath("//div/select/option")
  end

  def wait_for_manual_browser_to(action)
    case action
      when "provision","restore"
        status = /PROVISIONED/
      when "sleep"
        status = /SLEEP$/
    end

    timeout_in_min = @test_data["MANUAL_TEST_TIMEOUT"].to_i
    refresh = @test_data['MANUAL_TEST_REFRESH_INTERVAL'].to_i
    loops = (60/refresh)*timeout_in_min

    loops.times do |i|
      if @driver.find_element(:css => "tr[class='ng-scope']>td:nth-of-type(5)>div>div").text =~ status
        p "*** MANUAL BROWSER is SUCCESSFUL ***"
        return
      else
        # @driver.navigate.refresh
        sleep refresh
        p "=>=>=> #{i+1}. waiting <=<=<="
      end
    end
    raise("JOB did not complete in 15 mins")
  end

  def perform_action_on_browser_machine(action)
    case action
      when "delete"
        manual_delete_button.click
        sleep(1)
        @driver.find_element(xpath: "//button[text()='OK']").click
      when "sleep"
        manual_sleep_button.click
      when "restore"
        manual_restore_button.click
    end
  end

end