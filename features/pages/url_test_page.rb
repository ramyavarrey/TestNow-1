class URLTestPage < PageObject

  element :url_test_url, {name: "testURL"}
  element :url_text_to_search, {name: "testToSearch"}
  element :url_timeout, {name: "pageLoadTimeOut"}
  element :url_execution_efficient, {css: "input[value=vmReuse]"}
  element :url_execution_boost, {css: "input[value=boost]"}

  element :url_test_url_error_message, {css: "input#urltext~p"}
  element :url_text_to_search_error_message, {css: "input[name*='testToSearch']~p"}
  element :url_timeout_error_message, {css: "input[name*='pageLoadTimeOut']~p"}

  element :browser_chrome_button, {xpath: "//img[contains(@src,'Chrome')]/parent::button"}
  element :browser_firefox_button, {xpath: "//button[contains(text(),'Firefox')]"}
  element :browser_opera_button, {xpath: "//button[contains(text(),'Opera')]"}

  element :browser_opera_mini_button, {xpath: "//button[contains(text(),'OperaMini')]"}
  element :browser_opera_mobile_button, {xpath: "//button[contains(text(),'OperaMobile')]"}

  element :browser_ie_button, {xpath: "//button[contains(text(),'IE')]"}
  element :browser_android_button, {xpath: "//button[contains(text(),'Android:')]"}
  element :browser_android_chrome_button, {xpath: "//button[contains(text(),'AndroidChrome')]"}
  element :browser_upa_button, {xpath: "//button[contains(text(),'UIPerfAnalysis')]"}
  element :url_crawl_url_button, {xpath: "//input[@name='runCrawl']"}
  element :browser_device_button, {xpath: "//button[contains(text(),'Device')]"}

  element :url_submit_test, {id: "runUrlTest"}


  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def select_browser_version(browser_name, version)
    sleep(1)
    case browser_name.downcase
      when "firefox"
        browser_firefox_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_firefox_button.click
      when "opera"
        browser_opera_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_opera_button.click
      when "chrome"
        browser_chrome_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_chrome_button.click
      when "operamini"
        browser_opera_mini_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_opera_mini_button.click
      when "operamobile"
        browser_opera_mobile_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_opera_mobile_button.click
      when "ie"
        browser_ie_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_ie_button.click
      when "android"
        browser_android_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_android_button.click
      when "androidchrome"
        browser_android_chrome_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_android_chrome_button.click
      when "uiperfanalysis"
        browser_upa_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_upa_button.click
      when "device"
        browser_device_button.click
        calculate_and_select_browser_version(browser_name,version)
        browser_device_button.click
    end
    sleep(1)
  end

  def calculate_and_select_browser_version(browser_name,version)
    if version.include? ':'
      versions = version.split(':')
      versions.each do |v|
        calculate_and_select_browser_version(browser_name,v)
      end
    else
      if version == "RANDOM"
        version_to_select = get_random_version(browser_name)
      else
        version_to_select = version
      end
      #Create a MAP with browser and Version
      $bv_map[browser_name] = version_to_select

      case browser_name.downcase
        when "firefox","operamini","operamobile","android","ie","androidchrome","uiperfanalysis","device"
          @driver.find_element(xpath: "//button[contains(text(),'#{browser_name}')]/following-sibling::ul/li[@role='presentation']/a[contains(text(),'#{version_to_select}')]").click
        when "chrome"
          @driver.find_element(xpath: "//img[contains(@src,'Chrome')]/parent::button/following-sibling::ul/li[@role='presentation']/a[contains(text(),'#{version_to_select}')]").click
        when "opera"
          @driver.find_element(xpath: "//button[contains(text(),'Opera:')]/following-sibling::ul/li[@role='presentation']/a[contains(text(),'#{version_to_select}')]").click

      end
      p "----- #{browser_name} Selected : #{version_to_select} -----"
    end
  end

  def get_random_version(browser_name)
    case browser_name.downcase
      when "firefox","operamini","operamobile","android","ie","androidchrome","uiperfanalysis","device"
        versions = driver.find_elements(xpath: "//button[contains(text(),'#{browser_name}')]/following-sibling::ul/li[@role='presentation']/a")
        versions[rand(versions.size)].text
      when "opera"
        versions = driver.find_elements(xpath: "//button[contains(text(),'Opera')]/following-sibling::ul/li[@role='presentation']/a")
        versions[rand(versions.size)].text
      when "chrome"
        versions = driver.find_elements(xpath: "//img[contains(@src,'Chrome')]/parent::button/following-sibling::ul/li[@role='presentation']/a")
        versions[rand(versions.size)].text
    end
  end

  def fill_url_test_form
    url_test_url.clear
    url_test_url.send_keys(@test_data['URL_TEST_URL'])
    url_text_to_search.clear
    url_text_to_search.send_keys(@test_data['URL_TEST_TEXT'])
    url_execution_boost.click
    url_timeout.clear
    url_timeout.send_keys("7")
    sleep(1)
  end


  def fill_url_test_Efficient_form
    url_test_url.clear
    url_test_url.send_keys(@test_data['URL_TEST_URL'])
    url_text_to_search.clear
    url_text_to_search.send_keys(@test_data['URL_TEST_TEXT'])
    url_execution_efficient.click
    url_timeout.clear
    url_timeout.send_keys("7")
    sleep(1)
  end

  def fill_individual_field(value,field)
    case value
      when "space"
        key = "asd"
      when "alphabet"
        key = "abc"
      when "negative"
        key = -5
    end
    case field
      when "AppUrl"
        url_test_url.clear
        url_test_url.send_keys(key)
        url_test_url.clear
      when "TextToSearch"
        url_text_to_search.clear
        url_text_to_search.send_keys(key)
        url_text_to_search.clear
      when "Timeout"
        url_timeout.clear
        if key == "asd"
          url_timeout.send_keys("0")
        else
          url_timeout.send_keys(key)
         # url_timeout.clear
        end
    end
  end
  end

