class DashboardPage < PageObject

  element :dashboard_execution_time_widget, {xpath: "//div[contains(@comments,'Execution')]"}
  element :dashboard_fail_fast_widget, {xpath: "//div[contains(@comments,'Fail')]"}
  element :dashboard_test_passed_widget, {xpath: "//div[contains(@comments,'Passed')]"}
  element :dashboard_releasable_builds_widget, {xpath: "//div[contains(@comments,'Releasable')]"}

  element :dashboard_toggle_jobs_section, {id: "toggle-btn"}
  element :dashboard_builds_section, {id: "lastfewjobs"}

  element :consolidated_report_button, {id: "consolidatedReportButton"}
  element :scalenow_report_button, {id: "scaleNowReportButton"}
  element :scaleNow_report_heading, {xpath: ".//*[@id='testIp']/div/div/div[1]"}
  element :job_name_label,{xpath: ".//*[@id='testIp']/div/div/div[2]/div/div[1]/label" }
  element :test_criteria_label, {xpath: ".//label[text()='Test Criteria(input):']"}
  element :download_report_pdf_button , {id: "button"}
  element :search_textbox , {xpath: "//input[@placeholder='Search ...']"}

  def initialize(page_driver, test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def wait_for_job_to_complete(job_type,browser_count=1)
    case job_type
      when "Automated"
        timeout_in_mins = @test_data['AUTOMATED_TEST_TIMEOUT'].to_i
        refresh_interval = @test_data['AUTOMATED_TEST_REFRESH_INTERVAL'].to_i
      when "URL"
        timeout_in_mins = @test_data['URL_TEST_TIMEOUT'].to_i
        refresh_interval = @test_data['URL_TEST_REFRESH_INTERVAL'].to_i
      when "Security"
        timeout_in_mins = @test_data['SECURITY_TEST_TIMEOUT'].to_i
        refresh_interval = @test_data['SECURITY_TEST_REFRESH_INTERVAL'].to_i
      when "UPA"
        timeout_in_mins = @test_data['UPA_TEST_TIMEOUT'].to_i
        refresh_interval = @test_data['UPA_TEST_REFRESH_INTERVAL'].to_i
      when "Scale"
        timeout_in_mins = @test_data['SCALE_TEST_TIMEOUT'].to_i
        refresh_interval = @test_data['SCALE_TEST_REFRESH_INTERVAL'].to_i
    end
    loops = (60/refresh_interval)*timeout_in_mins
    p loops.to_s
    #links = ["URL Test","Manual Test","Security Test","UI Performance Analysis Test","Cross Browser","Rest API","Load Test", "Dashboard"]
    links = ["URL Test","Manual Test","Security Test","UI Performance Analysis Test","Cross Browser", "Dashboard"]
    loops.times do |i|
      success_count = @driver.find_element(:xpath => "//tr/td/span[contains(text(),'Success')]").text.split(" ")[1].to_i
      p "success count is:#{success_count}"
      p "----------------------------------------"
      p "browser count is:#{browser_count}"
      fail_count = @driver.find_element(:xpath => "//span[contains(text(),'Failed')]").text.split(" ")[1].to_i

      if job_type == "Automated" && (success_count + fail_count == browser_count.to_i)
        p "*** JOB is SUCCESSFUL ***"
        return
      elsif job_type == "URL" && (success_count == browser_count.to_i)
        p "*** JOB is SUCCESSFUL ***"
        return
      elsif job_type == "URL" && (fail_count > 0)
        p "*** FAILING THE TEST AND EXITING ***"
        fail("**** TESTNOW URL TEST FAILED IN 1 or MORE BROWSER ***")
        return false
      elsif job_type == "Security" && (success_count + fail_count == browser_count.to_i)
        p "*** JOB is SUCCESSFUL ***"
        return
      elsif job_type == "UPA" &&  (success_count + fail_count == browser_count.to_i)
        p "*** JOB is SUCCESSFUL ***"
        return
      elsif job_type == "Scale" &&  (success_count + fail_count == browser_count.to_i)
        p "*** JOB is SUCCESSFUL ***"
        return
      else
        @driver.navigate.refresh
       # sleep 30
=begin
        links.each do |page|
          common = CommonPage.new(@driver,@test_data)
          common.navigation(page)
          sleep 5
          common = CommonPage.new(@driver,@test_data)
          common.page_verification(page)
        end
 sleep 5
=end

        p "=>=>=> #{i+1}. waiting <=<=<="
      end
    end
    raise("JOB did not complete in 30 mins")
  end

  def verify_all_4_widgets_displayed
    dashboard_execution_time_widget.displayed?.should == true
    dashboard_fail_fast_widget.displayed?.should == true
    dashboard_test_passed_widget.displayed?.should == true
    dashboard_releasable_builds_widget.displayed?.should == true
  end

  def restart_job(keyword)
    jobs = @driver.find_elements(xpath: "//i[@class='fa fa-#{keyword}']/ancestor::tr//a[@id='playButton']/i")

    if jobs.size == 0
      "pending"
    else
      jobs.first.click
      sleep(3)
    end
  end

  def stop_job(keyword)
    jobs = @driver.find_elements(xpath: "//i[@class='fa fa-#{keyword}']/ancestor::tr/td[8]/a")

    if jobs.size == 0
      "pending"
    else
      jobs.first.click
      sleep(3)
    end
  end

  def verifyScaleNowReport
    download_report_pdf_button.text.should == "Download Report PDF"
    @driver.find_element(:xpath => ".//label[text()='"+@test_data['SCALENOW_APP_NAME']+"']").displayed?.should == true
    test_criteria_label.displayed?.should == true
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[1]/td[1]").text.should == "Application URL"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[2]/td[1]").text.should == "Parallel VMs Count"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[3]/td[1]").text.should == "Hours to Run"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[4]/td[1]").text.should == "Git Repo"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[5]/td[1]").text.should == "User ID"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[6]/td[1]").text.should == "Git branch"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[1]/td[2]").text.should == @test_data['SCALENOW_APP_URL']
    Integer(@driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[2]/td[2]").text).should == @test_data['SCALENOW_PARALLEL_USER_COUNT']
    Integer(@driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[3]/td[2]").text).should == @test_data['SCALENOW_HOURS_TO_RUN']
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[4]/td[2]").text.should == @test_data['SCALENOW_GIT_REPO']
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[5]/td[2]").text.should == @test_data['SCALENOW_USER_ID']
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr[6]/td[2]").text.should == @test_data['SCALENOW_GIT_BRANCH']

    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[1]/td[1]").text.should == "Test Run Command"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[2]/td[1]").text.should == "Pre - script"
   # @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[3]/td[1]").text.should == "Post - script"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[4]/td[1]").text.should == "Output Directory"
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[5]/td[1]").text.should == "Report File"
    sleep 3
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[1]/td[2]").text.should == @test_data['SCALENOW_TEST_RUN_COMMAND']
    sleep 3
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[2]/td[2]").text.should == @test_data['SCALENOW_PRESCRIPT']
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[4]/td[2]").text.should == @test_data['SCALENOW_TEST_OUTPUT_DIRECTORY']
    @driver.find_element(:xpath => ".//*[@id='testIp']/div/div/div[2]/div/div[2]/div/div/div[2]/table/tbody/tr[5]/td[2]").text.should == @test_data['SCALENOW_TEST_REPORT_FILE']

    svg = @driver.find_elements(:css => "svg")
    for i in 1..svg.size
      outerText = svg.first.find_element(:css => "text")
      outerText.text.should_not == "No Data Available."
    end
  end

  def search_job_id(job_name)
    case job_name.downcase
      when "url"
        keyword = "link"
      when "automated"
        keyword = "cogs"
      when "scalenow"
        keyword = "truck"
    end
    $search_job_id = driver.find_element(xpath: "//i[@class='fa fa-#{keyword}']/ancestor::tr/td[3]/a").text
    p "search job id is #{$search_job_id}"
    search_textbox.send_keys($search_job_id)
    sleep(2)
  end
end