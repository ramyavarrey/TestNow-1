class SecurityTestPage < PageObject
   element :application_url_text, {id: "urltext"}
   element :test_now_button, {id: "runUrlTest"}
   element :app_scan_checkbox, {xpath: "//form[@name='urlTestForm']/div[3]/div/div/div/label[1]/input"}
   element :http_header_checkbox, {xpath: "//form[@name='urlTestForm']/div[3]/div/div/div/label[2]/input"}
   element :app_scan_pack_label, {xpath: "//label[@id='app_scan_pack']"}
   element :app_scan_feature_heading, {xpath: "//div[@id='app_scan']/div/ul/b"}
   element :app_scan_feature1, {xpath: "//div[@id='app_scan']/div/ul/li[1]"}
    element :app_scan_feature2, {xpath: "//div[@id='app_scan']/div/ul/li[2]"}
    element :app_scan_feature3, {xpath: "//div[@id='app_scan']/div/ul/li[3]"}
    element :app_scan_feature4, {xpath: "//div[@id='app_scan']/div/ul/li[4]"}
    element :app_scan_feature5, {xpath: "//div[@id='app_scan']/div/ul/li[5]"}
    element :app_scan_feature6, {xpath: "//div[@id='app_scan']/div/ul/li[6]"}
    element :app_scan_feature7, {xpath: "//div[@id='app_scan']/div/ul/li[7]"}
    element :app_scan_feature8, {xpath: "//div[@id='app_scan']/div/ul/li[8]"}
    element :app_scan_feature9, {xpath: "//div[@id='app_scan']/div/ul/li[9]"}
    element :app_scan_feature10, {xpath: "//div[@id='app_scan']/div/ul/li[10]"}
    element :app_scan_feature11, {xpath: "//div[@id='app_scan']/div/ul/li[11]"}
    element :app_scan_feature12, {xpath: "//div[@id='app_scan']/div/ul/li[12]"}
    element :app_scan_feature13, {xpath: "//div[@id='app_scan']/div/ul/li[13]"}
    element :app_scan_feature14, {xpath: "//div[@id='app_scan']/div/ul/li[14]"}
    element :app_scan_feature15, {xpath: "//div[@id='app_scan']/div/ul/li[15]"}
    element :app_scan_feature16, {xpath: "//div[@id='app_scan']/div/ul/li[16]"}

   element :http_header_pack_label, {xpath: "//label[@id='http_headers_pack']"}
   element :http_header_feature_heading, {xpath: "//div[@id='http_headers']/div/ul/b"}
   element :http_header_feature1, {xpath: "//div[@id='http_headers']/div/ul/li[1]"}
    element :http_header_feature2, {xpath: "//div[@id='http_headers']/div/ul/li[2]"}
    element :http_header_feature3, {xpath: "//div[@id='http_headers']/div/ul/li[3]"}
    element :http_header_feature4, {xpath: "//div[@id='http_headers']/div/ul/li[4]"}
    element :http_header_feature5, {xpath: "//div[@id='http_headers']/div/ul/li[5]"}

   def initialize(page_driver, test_data)
       @driver = page_driver
       @test_data = test_data
     end

    def clear_application_url
        application_url_text.clear
    end

     def test_now_button_click
        test_now_button.click
        sleep(1)
     end

     def app_scan_click
        app_scan_checkbox.click
        sleep(2)
     end

     def http_header_click
        http_header_checkbox.click
        sleep(2)
     end
end