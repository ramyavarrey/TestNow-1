class UpaTestPage < PageObject
  element :application_url_text, {xpath: "//input[@name='testURL']"}
  element :text_to_search_text, {xpath: "//input[@name='testToSearch']"}
  element :timeout_text, {xpath: "//input[@name='pageLoadTimeOut']"}
  element :crawl_url_checkbox, {xpath: "//input[@name='runCrawl']"}
  element :test_now_button, {id: "runUrlTest"}
  element :upa_execution_efficient, {css: "input[value=vmReuse]"}
  element :upa_execution_boost, {css: "input[value=boost]"}

  def initialize(page_driver,test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def fill_upa_test_form
    clear_app_url
    application_url_text.send_keys(@test_data['UPA_Test_URL'])
    clear_text_to_search
    send_keys_text_to_search(@test_data['UPA_TEST_TEXT'])
    clear_timeouts
    send_keys_timeout("10")
    upa_execution_efficient.click
    crawl_url_checkbox.click
  end


  def fill_upa_test_boost_form
    clear_app_url
    application_url_text.send_keys(@test_data['UPA_Test_URL'])
    clear_text_to_search
    send_keys_text_to_search(@test_data['UPA_TEST_TEXT'])
    upa_execution_boost.click
    clear_timeouts
    send_keys_timeout("7")
    sleep(1)
  end


  def clear_app_url
    application_url_text.clear
  end


  def clear_text_to_search
    text_to_search_text.clear
  end

  def clear_timeouts
    timeout_text.clear
  end

  def send_keys_text_to_search(text)
    text_to_search_text.send_keys(text)
  end

  def send_keys_timeout(text)
    timeout_text.send_keys(text)
  end

end