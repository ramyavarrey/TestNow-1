class HomePage < PageObject

  element :signup_button, {xpath: "//a/span[text()='Sign Up']"}
  element :signup_name, {name: "firstName"}
  element :signup_email, {name: "email"}
  element :signup_password, {name: "password"}
  element :signup_organization, {name: "org"}
  element :signup_tnc_checkbox, {css: "input[ng-model=tandcFlag]"}
  element :signup_submit_button, {css: "button[ng-click*='signUp']"}

  element :signup_name_error_message, {xpath: "//form[@name='signupForm']/div[2]/p"}
  element :signup_email_error_message, {xpath: "//form[@name='signupForm']/div[3]/p"}
  element :signup_password_error_message, {xpath: "//form[@name='signupForm']/div[4]/p"}
  element :signup_organization_error_message, {xpath: "//form[@name='signupForm']/div[5]/p"}


  element :tnc_link, {link_text: "Terms and Conditions."}
  element :tnc_header, {xpath: "//div[@class='acc-pane']/p[1]"}
  element :tnc_close_button, {xpath: "//div[@class='modal-footer']/button[text()='Close']"}

  element :login_link, {xpath: "//ul[@class='list-inline intro-social-buttons']/li/a/span[text()='Login']"}
  element :login_username, {css: "form[name=loginForm] input[name=email]"}
  element :login_password, {css: "form[name=loginForm] input[name=password]"}
  element :login_button, {xpath: "//button[text()='Login']"}

  element :user_dropdown, {id: "userTab"}
  element :logout_button, {xpath: "//a[text()='Logout']"}

  element  :aws_access_key,  {xpath: "//input[@name='accessKey']"}
  element  :aws_secret_key,  {xpath: "//input[@name='secretKey']"}
  element  :aws_ownerid_key,  {xpath: "//input[@name='ownerId']"}
  element  :aws_report_expiration_key,  {xpath: "//input[@name='s3expirationDays']"}
  element  :skip_button,     {xpath: "//button[@id='skipButton']"}
  element  :save_button,  {xpath:"//button[contains(text(),'Save')]"}
  element   :aws_success_message,  {xpath:"//div[@class='message ng-binding'"}


  def initialize(page_driver,test_data)
    @driver = page_driver
    @test_data = test_data
  end

  def testnow_base_url
    if ENV['TEST_URL'].nil?
      @driver.get(@test_data['TESTNOW_URL'])
    else
      @driver.get(ENV['TEST_URL'])
    end
  end

  def fill_in_signup_form
    Selenium::WebDriver::Wait.new(timeout: 10).until {signup_submit_button.displayed?}
    signup_name.send_keys(Faker::Name.first_name)
    signup_email.send_keys(Faker::Internet.free_email)
    signup_password.send_keys("ZAQ!zaq1")
    signup_organization.send_keys("Opex Software")
    signup_tnc_checkbox.click
    sleep 2
  end

  def empty_aws_details
    Selenium::WebDriver::Wait.new(timeout: 10).until {save_button.displayed?}
    aws_access_key.clear
    aws_secret_key.clear
    aws_ownerid_key.clear
    aws_report_expiration_key.clear
  end

  def fill_aws_details
    Selenium::WebDriver::Wait.new(timeout: 10).until {save_button.displayed?}
    aws_access_key.send_keys(@test_data['AWS_ACCESS_KEY'])
    aws_secret_key.send_keys(@test_data['AWS_SECRET_KEY'])
    aws_ownerid_key.send_keys(@test_data['AWS_OWNER_ID'])
    aws_report_expiration_key.send_keys(2)
  end


  def fill_in_signup_invalid_form(type)
    case type
      when "less digit"
        Selenium::WebDriver::Wait.new(timeout: 10).until {signup_submit_button.displayed?}
        signup_name.send_keys(Faker::Name.first_name)
        signup_email.clear
        signup_email.send_keys(Faker::Internet.free_email)
        signup_password.send_keys("ZAQ!")
        signup_organization.send_keys("Opex Software")
        signup_tnc_checkbox.click
      when "more digit"
        Selenium::WebDriver::Wait.new(timeout: 10).until {signup_submit_button.displayed?}
        signup_name.send_keys(Faker::Name.first_name)
        signup_email.clear
        signup_email.send_keys(Faker::Internet.free_email)
        signup_password.send_keys("ZAQ!zaq1XSW@xsw2")
        signup_organization.send_keys("Opex Software")
        signup_tnc_checkbox.click
      when "alphanumeric"
        Selenium::WebDriver::Wait.new(timeout: 10).until {signup_submit_button.displayed?}
        signup_name.send_keys(Faker::Name.first_name)
        signup_email.clear
        signup_email.send_keys(Faker::Internet.free_email)
        signup_password.clear
        signup_password.send_keys("zaqzaqxsw")
        signup_organization.send_keys("Opex Software")
        signup_tnc_checkbox.click
    end
  end

  def fill_in_already_registered_signup_form
    Selenium::WebDriver::Wait.new(timeout: 10).until {signup_submit_button.displayed?}
    signup_name.send_keys(Faker::Name.first_name)
    signup_email.send_keys(@test_data['TESTNOW_USERNAME'])
    signup_password.send_keys("ZAQ!zaq1")
    signup_organization.send_keys("Opex Software")
    signup_tnc_checkbox.click
  end

  def fill_in_without_termsandconditions_signup_form
    Selenium::WebDriver::Wait.new(timeout: 10).until {signup_submit_button.displayed?}
    signup_name.send_keys(Faker::Name.first_name)
    signup_email.send_keys(Faker::Internet.free_email)
    signup_password.send_keys("ZAdfvsq1")
    signup_organization.send_keys("Opex Software")

    end

  def fill_in_login_form
    Selenium::WebDriver::Wait.new(timeout: 10).until {login_username.displayed?}
    login_username.clear
    login_username.send_keys(@test_data['TESTNOW_USERNAME'])
    login_password.clear
    login_password.send_keys(@test_data['TESTNOW_PASSWORD'])
  end

  def fill_in_invalid_login_form
    Selenium::WebDriver::Wait.new(timeout: 10).until {login_username.displayed?}
    login_username.clear
    login_username.send_keys("jhgjkh")
    login_password.clear
    login_password.send_keys("hhkhk")
  end

  def empty_signup_form
    Selenium::WebDriver::Wait.new(timeout: 10).until {signup_submit_button.displayed?}
    signup_name.clear
    signup_email.clear
    signup_password.clear
    signup_organization.clear
  
  end

  def fill_in_login_form_for_index(index)
    if index.nil?
      csvValues = ["Kookie",@test_data['TESTNOW_USERNAME'],"Opex"]
    else
      users = File.read(File.dirname(__FILE__) + "/../../data/users.csv").split("\n")
      csv = users[index.to_i]
      csvValues = csv.split(",")
    end

    Selenium::WebDriver::Wait.new(timeout: 10).until {login_username.displayed?}
    login_username.clear
    login_username.send_keys(csvValues[1])
    login_password.clear
    login_password.send_keys("ZAQ!zaq1")
  end

def fill_in_individual_field(value,field)
  case value
    when "space"
      key = "asd"
    when "alphabet"
      key = "abc"
  end
  case field
    when "Name"
      signup_name.clear
      signup_name.send_keys(key)
      signup_name.clear
    when "Email"
      signup_email.clear
      signup_email.send_keys(key)
      signup_email.clear
    when "Password"
      signup_password.clear
      signup_password.send_keys(key)
      signup_password.clear
    when "Organization"
      signup_organization.clear
      signup_organization.send_keys(key)
      signup_organization.clear

  end


end

  def fill_in_invalid_email(value)
    case value
      when "text"
        signup_email.clear
        signup_email.send_keys("text")

      when "hello@gmail."
        signup_email.clear
        signup_email.send_keys("hello@gmail.")
        sleep 2
      when "text@gmailcom"
        signup_email.clear
        signup_email.send_keys("text@gmailcom")
        sleep 2
      when "text@gmail"
        signup_email.clear
        signup_email.send_keys("text@gmail")
        sleep 2
      when "text@fdhghf"
        signup_email.clear
        signup_email.send_keys("text@fdhghf")
        sleep 2



    end
end


end