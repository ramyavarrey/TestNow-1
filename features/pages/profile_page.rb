class ProfilePage < PageObject

  element :profile_section, {xpath: "//div[contains(text(),'Profile')]"}
  element :referral_section, {xpath: "//div[contains(text(),'Referral Program')]"}

  element :profile_edit_button, {xpath: "//button[text()='Edit Profile']"}
  element :profile_submit_button, {}
  element :profile_change_password_button, {}
  element :profile_cancel_button, {}
  element :refer_button, {xpath: "//button[contains(text(),'Refer')]"}
  element :refer_email_id, {xpath: "//input[@name='referralEmail']"}


  def initialize(driver, test_data)
    @driver = driver
    @test_data = test_data
  end

  

  def verify_profile_data_displayed()
    labels = {"First Name" => @test_data['TESTNOW_NAME'],
              "Email" => @test_data['TESTNOW_USERNAME'],
              "Organization" => @test_data['TESTNOW_ORGANIZATION']}
    Selenium::WebDriver::Wait.new(timeout: 30).until{@driver.find_element(xpath: "//label[contains(text(),'First Name')]/following-sibling::div/p").text == @test_data['TESTNOW_NAME']}
    labels.each do |label,value|
      @driver.find_element(xpath: "//label[contains(text(),'#{label}')]/following-sibling::div/p").text.should == value
    end
  end

  def verify_profile_data(action)
    (action == :editable) ?
          @driver.find_element(css: "div.panel-body>div").displayed?.should == false :
          @driver.find_element(css: "div.panel-body>div").displayed?.should == true
  end

end

